/*
 * ANGELS
 * 
 * 3/22/18
 * Mark Travers
 * 
 * Receives a motor speed ratio as a byte.
 * Applies the ratio to the servos.
 */

#define servoPinRight 2
#define servoPinLeft 3

#define stoppedSpeedRight 1500
#define fullSpeedRight 1000

#define stoppedSpeedLeft 1500
#define fullSpeedLeft 2000

#include <Servo.h>
Servo servoRight;
Servo servoLeft;

unsigned int speedRatio = 255;
unsigned int pulseLengthRight = stoppedSpeedRight;
unsigned int pulseLengthLeft = stoppedSpeedLeft;

void setup() {
  pinMode(servoPinRight, OUTPUT);
  pinMode(servoPinLeft, OUTPUT);
  servoRight.attach(servoPinRight);
  servoLeft.attach(servoPinLeft);
  applySpeeds();
  Serial.begin(9600);
}

void loop() {

}

void applySpeeds() {
  if (speedRatio == 255) {
    pulseLengthRight = stoppedSpeedRight;
    pulseLengthLeft = stoppedSpeedLeft;
  }
  else {
    pulseLengthRight = map(speedRatio, 0, 254, fullSpeedRight, stoppedSpeedRight);
    pulseLengthLeft = map(speedRatio, 254, 0, fullSpeedLeft, stoppedSpeedLeft);
  }
  servoRight.writeMicroseconds(pulseLengthRight);
  servoLeft.writeMicroseconds(pulseLengthLeft);
}

void serialEvent() {
  while(Serial.available()) {
    speedRatio = Serial.parseInt();
  }
  applySpeeds();
}

