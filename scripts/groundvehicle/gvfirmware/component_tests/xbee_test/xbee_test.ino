/*
 * ANGELS
 * 
 * 8/10/2018
 * Mark Travers
 * 
 * Tests xBees by setting servo speed
 */

#define speedRange 500
#define speedStopped 1500
#define speedFactor 11.811023

#define servoPinLeft 2
#define servoPinRight 3

#define commandLength 2

#include <Servo.h>
Servo servoRight;
Servo servoLeft;

void setup() {
  pinMode(servoPinRight, OUTPUT);
  pinMode(servoPinLeft, OUTPUT);
  servoRight.attach(servoPinRight);
  servoLeft.attach(servoPinLeft);

  servoRight.writeMicroseconds(1500);
  servoLeft.writeMicroseconds(1500);

  Serial.begin(9600);

}

void loop() {

}

void setServos(unsigned char leftByte, unsigned char rightByte) {
  int leftPeriod = speedStopped;
  int rightPeriod = speedStopped;

  leftPeriod = speedStopped + speedFactor*(float)(leftByte - 127);
  rightPeriod = speedStopped - speedFactor*(float)(rightByte - 127);

  Serial.print("left Period: ");
  Serial.println(leftPeriod);
  Serial.print("right Period: ");
  Serial.println(rightPeriod);

  servoLeft.writeMicroseconds(leftPeriod);
  servoRight.writeMicroseconds(rightPeriod);
  
}

void serialEvent() {
  
  if (Serial.available() == commandLength) {
    unsigned char leftByte = 0;
    unsigned char rightByte = 0;
    
    leftByte = Serial.read();
    rightByte = Serial.read();

//    Serial.print("Left: ");
//    Serial.println(leftByte);
//    Serial.print("Right: ");
//    Serial.println(rightByte);

    setServos(leftByte, rightByte);
  }
}

