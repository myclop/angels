/*
 * ANGELS
 * 
 * 3/22/18
 * Mark Travers
 * 
 * Tests that the servos on the Boe-Bot function correctly
 */

#define servoPinRight 4
#define servoPinLeft 5

#include <Servo.h>
Servo servoRight;
Servo servoLeft;

void setup() {
  pinMode(servoPinRight, OUTPUT);
  pinMode(servoPinLeft, OUTPUT);
  servoRight.attach(servoPinRight);
  servoLeft.attach(servoPinLeft);

  servoRight.writeMicroseconds(1000);
  servoLeft.writeMicroseconds(2000);

}

void loop() {
//  servoRight.writeMicroseconds(1000);
//  servoLeft.writeMicroseconds(2000);

}
