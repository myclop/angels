/*
 * 3/23/18
 * Mark Travers
 * 
 * Sends a byte through the serial port every interval of time
 */

unsigned char number = 0;

void setup() {
  Serial.begin(9600);

}

void loop() {
  Serial.write(number);
  number++;
  delay (100);
}
