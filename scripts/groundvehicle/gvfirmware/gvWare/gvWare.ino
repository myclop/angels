/*
 * ANGELS
 * 
 * 3/22/18
 * Mark Travers
 * 
 * Receives (left,right) motor speeds
 * Applies the speeds to the servos.
 */

#define servoPinRight 2
#define servoPinLeft 3

#define fullBackRight 2000
#define stoppedSpeedRight 1500
#define fullSpeedRight 1000

#define fullBackLeft 1000
#define stoppedSpeedLeft 1500
#define fullSpeedLeft 2000

#include <Servo.h>
Servo servoRight;
Servo servoLeft;

unsigned int pulseLengthRight = stoppedSpeedRight;
unsigned int pulseLengthLeft = stoppedSpeedLeft;
volatile unsigned char leftByte = 127;
volatile unsigned char rightByte = 127;

void setup() {
  pinMode(servoPinRight, OUTPUT);
  pinMode(servoPinLeft, OUTPUT);
  servoRight.attach(servoPinRight);
  servoLeft.attach(servoPinLeft);
  Serial.begin(9600);
  applySpeeds();
}

void loop() {
  applySpeeds();
}

void applySpeeds() {
  int leftPeriod = map(leftByte, 0, 254, fullBackLeft, fullSpeedLeft);
  int rightPeriod = map(rightByte, 0, 254, fullBackRight, fullSpeedRight);
  servoLeft.writeMicroseconds(leftPeriod);
  servoRight.writeMicroseconds(rightPeriod);
}

void serialEvent() {
  if (Serial.available() >= 2) {
    leftByte = Serial.read();
    rightByte = Serial.read();
    while(Serial.available()) {
      Serial.read();
    }
    Serial.write(35);
  }
}
