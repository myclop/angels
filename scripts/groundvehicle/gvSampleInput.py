#!python3

from robot_simulator import twoWheelRobot as twr
import math
import numpy as np
import matplotlib.pyplot as plt
import msvcrt as m

if __name__ == '__main__':
    # def __init__ (self, width, initPosXY=(0.0,0.0), initHeading=0.0, initWheelSpeeds=(0.0,0.0)):
    angel = twr(0.1, (0.0,0.0), 0.0, (0.0,0.0))
    robotPoints = np.asarray(angel.getPos())

    plt.figure()
    plt.gca().set_aspect('equal', adjustable='box')
    plt.title('Robot Path')
    plt.xlabel('Distance')
    plt.ion()
    plt.show()

    for t in np.arange(0.0, 100.0, 0.1):
        control = input('Input a control\n')
        print (control)
        if control == '7':
            angel.moveRobot(0.1, (0.05,0.1))
        elif control == '8':
            angel.moveRobot(0.1, (0.1,0.1))
        elif control == '9':
            angel.moveRobot(0.1, (0.1,0.05))
        else:
            print ('FAIL!!!!')

        robotPoints = np.vstack((robotPoints, np.asarray(angel.getPos())))
        plt.scatter(robotPoints[:,0], robotPoints[:,1])
        plt.draw()
        plt.pause(0.001)


    # for t in np.arange(0.0, 20.0, 0.1):
    #     angel.moveRobot(0.1,(0.1,0.1)) # straight
    #     # angel.moveRobot(0.1,(0.1,0.05)) # right
    #     # angel.moveRobot(0.1,(0.05,0.1)) # left
    #     robotPoints = np.vstack((robotPoints, np.asarray(angel.getPos())))
    #     plt.scatter(robotPoints[:,0], robotPoints[:,1])
    #     plt.show()

    # for t in np.arange(0.0, 10.0, 0.1):
    #     angel.moveRobot(0.1,(0.1,0.05))
    #     robotPoints = np.vstack((robotPoints, np.asarray(angel.getPos())))
    #     plt.scatter(robotPoints[:,0], robotPoints[:,1])
    #     plt.show()

    # for t in np.arange(0.0, 6.0, 0.1):
    #     angel.moveRobot(0.1,(0.05,0.1))
    #     robotPoints = np.vstack((robotPoints, np.asarray(angel.getPos())))
    #     plt.scatter(robotPoints[:,0], robotPoints[:,1])
    #     plt.show()


    # plt.scatter(robotPoints[:,0], robotPoints[:,1])

    plt.show()
    print('done')

    
        
