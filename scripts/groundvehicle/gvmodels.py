#!python3

# ANGELS
#
# 8/28/2018
# Mark Travers
#
# This module contains 2 classes:
#   BoeBotGV - Interfaces to a two-servo-wheel robot (Parallex Boe-Bot chassis) via RS232

import numpy as np
import serial

class BoeBotGV:
    def __init__(self, mp, gui, commPort, baudRate, serialTimeout):
        self.mp = mp
        self.gui = gui
        self.boeBot = serial.Serial(commPort, baudRate, timeout=serialTimeout)
        self.mp.out(['GV', 'Status'], 'Connecting to Boe-Bot...')
        # Wait for a response from the robot
        while not self.boeBot.read():
            self.boeBot.write([127, 127])
        self.mp.out(['GV', 'Status'], 'Connected.')

    def moveGV(self, wheelEfforts):
        # Apply hardware gain (kh) and offset (127)
        wheelSpeeds = [(int(wheelEffort * self.gui.kh) + 127) for wheelEffort in wheelEfforts]
        # Send wheel speeds to the Boe-Bot
        self.boeBot.write(wheelSpeeds[::-1])
        # Wait for read receipt (#)
        self.boeBot.read()
        self.mp.out(['GV'], 'Left Wheel: %d    Right Wheel: %d' % (wheelSpeeds[0], wheelSpeeds[1]))

class SimulatedGV:
    def __init__(self, mp, gui, initPosYX=(0,0), initHeading=0.0):
        # Message pipe instanse
        self.mp = mp
        # Gui instance
        self.gui = gui

        # GV parameters
        self.width = self.gui.gvPixelRadius
        self._pos = initPosYX
        self._heading = initHeading
        # GV history: Position, Distance Error, Heading, Heading Error
        self.travelHistory = [[self._pos, 0.0, self.heading, 0.0]]
        self._dt = 0.1
        
        # Workspace variables
        self.dLeft = 0.0
        self.dRight = 0.0
        self.dGV = 0.0
        self.rGV = 0.0
        self.dTheta = 0.0
        self.headingNew = 0.0
        self.xGVNew = 0.0
        self.yGVNew = 0.0
        self.thetaO = 0.0
        self.thetaF = 0.0
        self.xCircle = 0.0
        self.yCircle = 0.0

    # Getters and Setters
    @property
    def pos(self):
        return(self._pos)
    @pos.setter
    def pos(self, yx):
        self._pos = yx
    @property
    def heading(self):
        return(self._heading)
    @heading.setter
    def heading(self, heading):
        self._heading = heading
    @property
    def dt(self):
        return(self._dt)
    @dt.setter
    def dt(self, dt):
        self._dt = dt

    def moveGV(self, wheelEfforts):
        wheelSpeeds = [wheelEffort * self.gui.kh for wheelEffort in wheelEfforts]
        (self.pos, self.heading) = self.calcNewPos(wheelSpeeds)
        self.gui.gvCoords = (int(self.pos[0]), int(self.pos[1]))
        self.gui.gvHeading = self.heading
        self.mp.out(['GV'], 'Left Wheel: %d    Right Wheel: %d' % (wheelSpeeds[0], wheelSpeeds[1]))

    # Functions
    def calcNewPos (self, wheelSpeeds):
        # Enforce theta domain for current heading
        if (self.heading < -np.pi):
            self.heading = self.heading + 2.0*np.pi
        elif (self.heading > np.pi):
            self.heading = self.heading - 2.0*np.pi

        # Calculate the distance that the center of the GV travels
        self.dLeft = self.dt * wheelSpeeds[0]
        self.dRight = self.dt * wheelSpeeds[1]
        self.dGV = 0.5 * (self.dLeft + self.dRight)

        # If the GV is turning...
        if (self.dLeft != self.dRight):
            # Calculate the turn radius from the center of the GV
            self.rGV = abs((0.5 * (self.dLeft + self.dRight) * self.width) / (self.dLeft - self.dRight))
            # Calculate the angle swept by the GV as it travels
            self.dTheta = self.dGV / self.rGV

        # Straight Case
        if (self.dLeft==self.dRight):
            # print('Straight')
            self.headingNew = self.heading
            self.xGVNew = self.pos[1] + self.dGV * np.cos(self.heading)
            self.yGVNew = self.pos[0] + self.dGV * np.sin(self.heading)

        # Contact Mark Travers (mark.2.travers@ucdenver.edu) for details about the Heading/Turning cases
        # Heading/Turning Case 1
        elif (self.heading>0.0) and (self.heading<np.pi/2.0) and (self.dLeft>self.dRight):
            # print('State 1')
            self.thetaO = self.heading - 0.5*np.pi
            self.thetaF = self.thetaO - self.dTheta
            self.xCircle = self.pos[1] + self.rGV * np.cos(self.thetaO)
            self.yCircle = self.pos[0] + self.rGV * np.sin(self.thetaO)
            self.xGVNew = self.xCircle - self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle - self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading - self.dTheta

        # Heading/Turning Case 2
        elif (self.heading>0.0) and (self.heading<np.pi/2.0) and (self.dLeft<self.dRight):
            # print('State 2')
            self.thetaO = self.heading - 0.5*np.pi
            self.thetaF = self.thetaO + self.dTheta
            self.xCircle = self.pos[1] - self.rGV * np.cos(self.thetaO)
            self.yCircle = self.pos[0] - self.rGV * np.sin(self.thetaO)
            self.xGVNew = self.xCircle + self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle + self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading + self.dTheta

        # Heading/Turning Case 3
        elif (self.heading>np.pi/2.0) and (self.heading<np.pi) and (self.dLeft>self.dRight):
            # print('State 3')
            self.thetaO = self.heading - 0.5*np.pi
            self.thetaF = self.thetaO - self.dTheta
            self.xCircle = self.pos[1] + self.rGV * np.cos(self.thetaO)
            self.yCircle = self.pos[0] + self.rGV * np.sin(self.thetaO)
            self.xGVNew = self.xCircle - self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle - self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading - self.dTheta

        # Heading/Turning Case 4
        elif (self.heading>np.pi/2.0) and (self.heading<np.pi) and (self.dLeft<self.dRight):
            # print('State 4')
            self.thetaO = self.heading - 0.5*np.pi
            self.thetaF = self.thetaO + self.dTheta
            self.xCircle = self.pos[1] - self.rGV * np.cos(self.thetaO)
            self.yCircle = self.pos[0] - self.rGV * np.sin(self.thetaO)
            self.xGVNew = self.xCircle + self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle + self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading + self.dTheta

        # Heading/Turning Case 5
        elif (self.heading>-np.pi/2.0) and (self.heading<0.0) and (self.dLeft<self.dRight):
            # print('State 5')
            self.thetaO = self.heading + 0.5*np.pi
            self.thetaF = self.thetaO + self.dTheta
            self.xCircle = self.pos[1] + self.rGV * np.cos(self.thetaO)
            self.yCircle = self.pos[0] + self.rGV * np.sin(self.thetaO)
            self.xGVNew = self.xCircle - self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle - self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading + self.dTheta

        # Heading/Turning Case 6
        elif (self.heading>-np.pi/2.0) and (self.heading<0.0) and (self.dLeft>self.dRight):
            # print('State 6')
            self.thetaO = self.heading + 0.5*np.pi
            self.thetaF = self.thetaO - self.dTheta
            self.xCircle = self.pos[1] - self.rGV * np.cos(self.thetaO)
            self.yCircle = self.pos[0] - self.rGV * np.sin(self.thetaO)
            self.xGVNew = self.xCircle + self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle + self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading - self.dTheta

        # Heading/Turning Case 7
        elif (self.heading>-np.pi) and (self.heading<-np.pi/2.0) and (self.dLeft<self.dRight):
            # print('State 7')
            self.thetaO = self.heading + 0.5*np.pi
            self.thetaF = self.thetaO + self.dTheta
            self.xCircle = self.pos[1] + self.rGV * np.cos(self.thetaO)
            self.yCircle = self.pos[0] + self.rGV * np.sin(self.thetaO)
            self.xGVNew = self.xCircle - self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle - self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading + self.dTheta

        # Heading/Turning Case 8
        elif (self.heading>-np.pi) and (self.heading<-np.pi/2.0) and (self.dLeft>self.dRight):
            # print('State 8')
            self.thetaO = self.heading + 0.5*np.pi
            self.thetaF = self.thetaO - self.dTheta
            self.xCircle = self.pos[1] - self.rGV * np.cos(self.thetaO)
            self.yCircle = self.pos[0] - self.rGV * np.sin(self.thetaO)
            self.xGVNew = self.xCircle + self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle + self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading - self.dTheta

        # Heading/Turning Case 9
        elif (self.heading==np.pi/2.0) and (self.dLeft>self.dRight):
            # print('State 9')
            self.thetaO = np.pi
            self.thetaF = self.thetaO - self.dTheta
            self.xCircle = self.pos[1] + self.rGV
            self.yCircle = self.pos[0]
            self.xGVNew = self.xCircle + self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle + self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading - self.dTheta

        # Heading/Turning Case 10
        elif (self.heading==-np.pi/2.0) and (self.dLeft>self.dRight):
            # print('State 10')
            self.thetaO = 0
            self.thetaF = self.thetaO - self.dTheta
            self.xCircle = self.pos[1] - self.rGV
            self.yCircle = self.pos[0]
            self.xGVNew = self.xCircle + self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle + self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading - self.dTheta

        # Heading/Turning Case 11
        elif (self.heading==np.pi/2.0) and (self.dLeft<self.dRight):
            # print('State 11')
            self.thetaO = 0
            self.thetaF = self.thetaO + self.dTheta
            self.xCircle = self.pos[1] - self.rGV
            self.yCircle = self.pos[0]
            self.xGVNew = self.xCircle + self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle + self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading + self.dTheta

        # Heading/Turning Case 12
        elif (self.heading==-np.pi/2.0) and (self.dLeft<self.dRight):
            # print('State 12')
            self.thetaO = 0
            self.thetaF = self.thetaO + self.dTheta
            self.xCircle = self.pos[1] + self.rGV
            self.yCircle = self.pos[0]
            self.xGVNew = self.xCircle - self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle - self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading + self.dTheta

        # Heading/Turning Case 13
        elif (self.heading==0.0) and (self.dLeft<self.dRight):
            # print('State 13')
            self.thetaO = -np.pi/2.0
            self.thetaF = self.thetaO + self.dTheta
            self.xCircle = self.pos[1]
            self.yCircle = self.pos[0] + self.rGV
            self.xGVNew = self.xCircle + self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle + self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading + self.dTheta

        # Heading/Turning Case 14
        elif (self.heading==0.0) and (self.dLeft>self.dRight):
            # print('State 14')
            self.thetaO = np.pi/2.0
            self.thetaF = self.thetaO - self.dTheta
            self.xCircle = self.pos[1]
            self.yCircle = self.pos[0] - self.rGV
            self.xGVNew = self.xCircle + self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle + self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading - self.dTheta

        # Heading/Turning Case 15
        elif ((self.heading==np.pi) or (self.heading==-np.pi)) and (self.dLeft>self.dRight):
            # print('State 15')
            self.thetaO = np.pi/2.0
            self.thetaF = self.thetaO - self.dTheta
            self.xCircle = self.pos[1]
            self.yCircle = self.pos[0] + self.rGV
            self.xGVNew = self.xCircle - self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle - self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading - self.dTheta

        # Heading/Turning Case 16
        elif ((self.heading==np.pi) or (self.heading==-np.pi)) and (self.dLeft<self.dRight):
            # print('State 16')
            self.thetaO = -np.pi/2.0
            self.thetaF = self.thetaO + self.dTheta
            self.xCircle = self.pos[1]
            self.yCircle = self.pos[0] - self.rGV
            self.xGVNew = self.xCircle - self.rGV * np.cos(self.thetaF)
            self.yGVNew = self.yCircle - self.rGV * np.sin(self.thetaF)
            self.headingNew = self.heading + self.dTheta

        # Throw an error if angle doesn't fit in any range
        else:
            raise Exception('Heading Error: out of range (%s)' % str(self.heading))

        # Enforce theta domain for new heading
        if (self.headingNew < -np.pi):
            return ((self.yGVNew, self.xGVNew), self.headingNew + 2.0*np.pi)
        elif (self.headingNew > np.pi):
            return ((self.yGVNew, self.xGVNew), self.headingNew - 2.0*np.pi)
        else:
            return ((self.yGVNew, self.xGVNew), self.headingNew)
