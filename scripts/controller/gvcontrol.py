#!python3

# ANGELS
# Senior Design Project
# Authors: Jason Carruth, Nicholas Hampton, Mark Travers
#
# Fall 2018
#
# This module contains three classes:
#   GVPilot - Implements a dual PID controller to guide the ground vehicle along its commanded path
#   PVPath - Implements an aStar pathing algorithm to generate an efficient path between the ground vehicle
#       and its destination.
#   PathNode - Stores information about an aStar node in the environment

import numpy as np
from scipy.spatial import distance

class GVPilot:
    def __init__(self, mp, gui, gvInstance, navigator, maxWheelEffort):
        # Message pipe instance
        self.mp = mp
        self.mp.out(['Pilot', 'Status'], 'Initializing pilot...')
        # Gui instance
        self.gui = gui
        # Ground vehicle instance
        self.gv = gvInstance
        # Navigator (pathing) instance
        self.navigator = navigator

        # Ground vehicle properties
        self._wheelEfforts = [0.0, 0.0]
        self.maxWheelEffort = maxWheelEffort
        # PID accumulators
        self.distanceErrorSum = 0.0
        self.thetaErrorSum = 0.0
        self.distanceErrorDerivative = 0.0

    # Getter and setter for ground vehicle wheel efforts
    @property
    def wheelEfforts(self):
        return(self._wheelEfforts)
    @wheelEfforts.setter
    def wheelEfforts(self, wheelEfforts):
        self._wheelEfforts = wheelEfforts
        # Slow down right wheel if left is maxed out
        if wheelEfforts[0] > self.maxWheelEffort:
            self.mp.out(['Pilot'], 'Clipping left wheel speed!')
            self._wheelEfforts[1] -= wheelEfforts[0] - self.maxWheelEffort
            self._wheelEfforts[0] = self.maxWheelEffort
        # Slow down left wheel if right is maxed out
        elif wheelEfforts[1] > self.maxWheelEffort:
            self.mp.out(['Pilot'], 'Clipping right wheel speed!')
            self._wheelEfforts[0] -= wheelEfforts[1] - self.maxWheelEffort
            self._wheelEfforts[1] = self.maxWheelEffort
        # Cap the backwards speed
        if wheelEfforts[0] < -self.maxWheelEffort + 0.1:
            self._wheelEfforts[0] = -self.maxWheelEffort + 0.1
        elif wheelEfforts[1] < -self.maxWheelEffort + 0.1:
            self._wheelEfforts[1] = -self.maxWheelEffort + 0.1

    # Determines the vehicle's distance and theta error and calculates a corrective wheel effort.
    def driveGV(self, gvSpeed):
        self.mp.out(['Pilot'], 'Generating driving commands...')
        # Find the closest node and the next node on the commanded path
        closestNode, closestNodeDistance, nextNode, segmentDistance, lastNodeFlag = self.navigator.findNearestPathSegment(self.gui.gvCoords)
        # Stops the ground vehicle if it has reached the destination
        if lastNodeFlag:
            self.mp.out(['Pilot', 'Status'], 'Arriving at final path node.')
            self.wheelEfforts = [0.0, 0.0]
            self.gui.gvTraveling = False
        else:
            # Translate d0 and d1 to the origin
            d0x = nextNode.pixelCoords[1] - closestNode.pixelCoords[1]
            d0y = nextNode.pixelCoords[0] - closestNode.pixelCoords[0]
            d1x = self.gui.gvCoords[1] - closestNode.pixelCoords[1]
            d1y = self.gui.gvCoords[0] - closestNode.pixelCoords[0]
            # Calculate dot product of d0 and d1
            d0Dotd1 = d0x*d1x + d0y*d1y
            # Determine what side of the segment the ground vehicle is on (steering direction)
            d0Theta = np.arctan2(d0y, d0x)
            d1Theta = np.arctan2(d1y, d1x)
            if d0Theta == np.pi and d1Theta <= 0:
                d0Theta = -np.pi
            nodeGVTheta = d1Theta - d0Theta
            if nodeGVTheta >= np.pi:
                nodeGVTheta = -(2.0*np.pi) + nodeGVTheta
            elif nodeGVTheta <= -np.pi:
                nodeGVTheta = (2.0*np.pi) + nodeGVTheta
            distanceError = np.sqrt(np.square(closestNodeDistance) - np.square(d0Dotd1/segmentDistance))
            # Positive error needs to turn right; Negative error needs to turn left
            if nodeGVTheta < 0.0:
                distanceError = -distanceError
                
            # Calculate the heading error (heading of the gv versus the heading of the path segment)
            headingError = self.gui.gvHeading - d0Theta
            if headingError > np.pi:
                headingError = -(2.0*np.pi) + headingError
            elif headingError < -np.pi:
                headingError = (2.0*np.pi) + headingError
            # Error accumulators (for PID integral term)
            self.distanceErrorSum += distanceError
            self.thetaErrorSum += headingError 

            # Cap commanded speed
            if gvSpeed > self.maxWheelEffort:
                gvSpeed = self.maxWheelEffort

            # Smooth distance error derivative by accumulative decay (manually tune the constant at the front of this expression)
            self.distanceErrorDerivative = 0.5*self.distanceErrorDerivative + (distanceError - self.gui.travelHistory[-1][1])
            
            # Calculate D-PID terms
            dpPID = self.gui.dkp * distanceError
            diPID = self.gui.dki * self.distanceErrorSum
            ddPID = self.gui.dkd * self.distanceErrorDerivative
            # Calculate Theta-PID terms
            thetapPID = self.gui.thetakp * headingError
            thetaiPID = self.gui.thetaki * self.thetaErrorSum
            thetadPID = self.gui.thetakd * (headingError - self.gui.travelHistory[-1][3])
            # Mix PIDs
            dPID = dpPID + diPID + ddPID
            thetaPID = thetapPID + thetaiPID + thetadPID
            totalPID = self.gui.dFactor * dPID + self.gui.thetaFactor * thetaPID

            # Positive PID goes right; Negative PID goes left
            self.wheelEfforts = [gvSpeed + totalPID, gvSpeed - totalPID]

            # Stops the ground vehicle if it is too far off the path (project requirement)
            if abs(distanceError) >= self.gui.gvPixelRadius / 2:
                self.mp.out(['Non-Fatal Error', 'Pilot', 'Status'], 'Emergency Stop: Too far off path!')
                self.wheelEfforts = [0.0, 0.0]
                self.gui.gvTraveling = False
                self.gui.destCoords = (None, None)

            self.mp.out(['PID'], 'd-Error: %d    p:%f    i:%f    d:%f    p:%f    i:%f    d:%f    heading:%f    PID:%f' % (distanceError, dpPID, diPID, ddPID, thetapPID, thetaiPID, thetadPID, self.gui.gvHeading, totalPID))

            # Record current GV state
            self.gui.travelHistory.append([self.gui.gvCoords, distanceError, self.gui.gvHeading, headingError])
        # Send commands to ground vehicle
        self.gv.moveGV(self.wheelEfforts)

class GVPath:
    def __init__(self, mp, gui, cameraResolution, pixelToNodeFactor):
        # Message pipe instance
        self.mp = mp
        # Gui instance
        self.gui = gui
        # Pixel to node scaling factor (pixels/node)
        self.pixelToNodeFactor = pixelToNodeFactor

        self.mp.out(['Pathing', 'Status'], 'Allocating aStar nodes...')

        # Down-sample pixels to create sparser node grid
        self.nodeGridSize = (len(range(0, cameraResolution[0], self.pixelToNodeFactor)), len(range(0, cameraResolution[1], self.pixelToNodeFactor)))
        # Allocate matrix of nodes
        self.nodeGrid = np.empty(self.nodeGridSize, dtype=PathNode)
        # Destination node
        self.destinationNode = None
        
        # Vector which stores the nodes on the path
        self.pathNodeList = []
        # Vector which stores the pixel coordinates on the path
        self.pathPixelCoords = []

    # Computes an efficent path from origin to destination using the aStar algorithm
    def findPath(self):
        # Instantiate nodes
        self.mp.out(['Pathing', 'Status'], 'Created a node grid of shape %s' % str(self.nodeGrid.shape))
        for i in range(self.nodeGridSize[0]):
            for j in range(self.nodeGridSize[1]):
                self.nodeGrid[i,j] = PathNode([i*self.pixelToNodeFactor, j*self.pixelToNodeFactor], [i,j], False, False, False, False)
        # Set obstacle states
        self.pathNodeList = []
        for i in range(self.nodeGridSize[0]): # Lines
            for j in range(self.nodeGridSize[1]): # Columns
                # Check to see if the current node overlaps an obstacle
                if self.gui.obstacleDrawing[i*self.pixelToNodeFactor, j*self.pixelToNodeFactor] > 127:
                    self.nodeGrid[i,j].obstacleFlag = True
                    # self.pathNodeList.append(self.nodeGrid[i,j])
                else:
                    self.nodeGrid[i,j].obstacleFlag = False
        # Set start and destination nodes
        currentNode = self.findNearestGridNode(self.gui.gvCoords)
        currentNode.originFlag = True
        self.destinationNode = self.findNearestGridNode(self.gui.destCoords)
        self.destinationNode.destinationFlag = True
        # Check that the current node is not an obstacle or the destination
        if currentNode.obstacleFlag or currentNode.destinationFlag:
            self.mp.out(['Non-Fatal Error', 'Pathing'], 'GV Position: obstacle-%s    destination-%s' % (currentNode.obstacleFlag, currentNode.destinationFlag))
            self.gui.gvTraveling = False
        else:
            self.pathNodeList.append(self.destinationNode)
            currentNode.gCost = 0
            currentNode.hCost = int(distance.euclidean(currentNode.nodeCoords, self.destinationNode.nodeCoords) * 10.0)
            # Initialize open and closed sets
            openNodes = [currentNode]
            closedNodes = []

            # Perform aStar until current node is the destination
            while not currentNode.destinationFlag:
                # Find all current node's eligible neighbors
                neighborNodeList = self.findNeighborNodes(currentNode)
                for neighborNode in neighborNodeList:
                    # If this node is previously unseen
                    if neighborNode.parent == None:
                        # Add this new node to the open set
                        openNodes.append(neighborNode)
                        # Take this node as a child
                        neighborNode.parent = currentNode
                        # Calculate the costs of this new node (fCost is calculated automatically)
                        (gCost, hCost, _) = self.calcCosts(currentNode, neighborNode)
                        neighborNode.gCost = gCost
                        neighborNode.hCost = hCost
                    # Else this neighbor has been seen, but not explored
                    else:
                        # Calculate the costs of this node if the current node became its parent
                        (gCost, hCost, fCost) = self.calcCosts(currentNode, neighborNode)
                        # Take the node as a child if the costs are less
                        if fCost < neighborNode.fCost:
                            neighborNode.parent = currentNode
                            neighborNode.gCost = gCost
                            neighborNode.hCost = hCost

                # This fails if there is no path between the current node and the destination
                if len(openNodes) > 0:
                    # Move the current node from the open set to the closed set
                    openNodes.remove(currentNode)
                    closedNodes.append(currentNode)
                    currentNode.closedFlag = True

                    # Find a node in the open set with the lowest f-cost for the next iteration
                    lowestCost = np.Infinity
                    for node in openNodes:
                        if node.originFlag:
                            raise Exception('man...')
                        if node.fCost < lowestCost:
                            lowestCost = node.fCost
                            currentNode = node
                # If there is no connection, a warning is given, and the destination is set to the closest node
                else:
                    self.mp.out(['Pathing', 'Non-Fatal Error'], 'No path between ground vehicle and destination!')
                    currentNode.destinationFlag = True

            # Trace back from the destination to compile a list of nodes along the path
            while not currentNode.originFlag:
                currentNode = currentNode.parent
                self.pathNodeList.append(currentNode)

            # Reverse the list of nodes and extract pixel coordinates from nodes
            self.pathNodeList = self.pathNodeList[::-1]
            self.pathPixelCoords = []
            for pathNode in self.pathNodeList:
                self.pathPixelCoords.append(pathNode.pixelCoords)
            # Passes node list to gui (nodes rather than points because of different node colors)
            self.gui.pathNodeList = self.pathNodeList

            # Set a flag to allow the ground vehicle to start moving
            self.gui.gvTraveling = True
            self.mp.out(['Pathing', 'Status'], 'New path generated.')

    # Find node nearest to a pair of pixel coordinates (indices are reversed to convert to matrix notation)
    def findNearestGridNode(self, pixelCoords):
        i = int(np.floor(pixelCoords[0] / self.pixelToNodeFactor))
        j = int(np.floor(pixelCoords[1] / self.pixelToNodeFactor))
        return (self.nodeGrid[i,j])

    # Find path segment nearest to a pair of pixel coordinates
    def findNearestPathSegment(self, gvCoords):
        lastNodeFlag = False
        closestNodeDistance = np.Infinity
        closestNodeIndex = 0
        for (nodeIndex, pathNode) in enumerate(self.pathNodeList):
            nodeDistance = distance.euclidean(gvCoords, pathNode.pixelCoords)
            if nodeDistance <= closestNodeDistance:
                closestNodeIndex = nodeIndex
                closestNodeDistance = nodeDistance
        # Prepare to stop navigation if endpoint is reached
        if len(self.pathNodeList) <= closestNodeIndex + 1:
            self.gui.reachDestination()
            lastNodeFlag = True
            segmentDistance = 0.0
            return(self.pathNodeList[closestNodeIndex], closestNodeDistance, self.pathNodeList[closestNodeIndex], segmentDistance, lastNodeFlag)
        else:
            segmentDistance = distance.euclidean(self.pathNodeList[closestNodeIndex].pixelCoords, self.pathNodeList[closestNodeIndex + 1].pixelCoords)
            return(self.pathNodeList[closestNodeIndex], closestNodeDistance, self.pathNodeList[closestNodeIndex + 1], segmentDistance, lastNodeFlag)


    # Find a set of eligible neighbors for a given node
    def findNeighborNodes(self, currentNode):
        neighborNodeList = []
        for i in [-1, 0, 1]:
            for j in [-1, 0, 1]:
                # Don't add the node itself
                if ((i != 0) or (j != 0)):
                    iNeighbor = currentNode.nodeCoords[0] + i
                    jNeighbor = currentNode.nodeCoords[1] + j
                    # Don't add nodes above or below the node space
                    if (iNeighbor >= 0) and (iNeighbor < self.nodeGridSize[0]):
                        # Don't add nodes to the left or right of the node space
                        if (jNeighbor >= 0) and (jNeighbor < self.nodeGridSize[1]):
                            neighbor = self.nodeGrid[iNeighbor, jNeighbor]
                            # Don't add nodes that are obstacles or the origin or nodes in the closed set
                            if not neighbor.obstacleFlag and not neighbor.originFlag and not neighbor.closedFlag:
                                # Add the node to the neighbor list
                                neighborNodeList.append(self.nodeGrid[iNeighbor,jNeighbor])
        return neighborNodeList

    # Calculate the g, h, and f costs of a node relative to another node
    def calcCosts(self, currentNode, neighborNode):
        gCost = currentNode.gCost + int(distance.euclidean(currentNode.nodeCoords, neighborNode.nodeCoords) * 10)
        hCost = int(distance.euclidean(self.destinationNode.nodeCoords, neighborNode.nodeCoords) * 10)
        fCost = gCost + hCost
        return (gCost, hCost, fCost)

class PathNode:
    def __init__(self, pixelCoords, nodeCoords, originFlag, destinationFlag, obstacleFlag, closedFlag):
        # Coordinates of the node (in the pixel grid)
        self.pixelCoords = pixelCoords
        # Coordinates of the node (in the node grid)
        self.nodeCoords = nodeCoords
        # True if this node is the origin
        self.originFlag = originFlag
        # True if this node is the destination
        self.destinationFlag = destinationFlag
        # True if this node is an obstacle
        self.obstacleFlag = obstacleFlag
        # True if this node is in the closed aStar set
        self.closedFlag = closedFlag
        # Pointer to the node which 'finds' this one
        self.parent = None
        # aStar parameters
        self._gCost = np.Infinity
        self._hCost = np.Infinity

    @property
    def gCost(self):
        return(self._gCost)
    @gCost.setter
    def gCost(self, gCost):
        self._gCost = gCost
    @property
    def hCost(self):
        return(self._hCost)
    @hCost.setter
    def hCost(self, hCost):
        self._hCost = hCost
    @property
    def fCost(self):
        return(self.gCost + self.hCost)
