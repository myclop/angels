#!python3

# ANGELS
# Senior Design Project
# Authors: Jason Carruth, Nicholas Hampton, Mark Travers
#
# Fall 2018
#
# This module has three classes:
#   Camera - interfaces to a physical camera
#   SimulatedCamera - Simulates a camera by drawing a white dot on a background picture 
#       to represent the ground vehicle. This allows the user to test ANGELS without physical hardware.
#   Analyst - Analyzes a camera frame to extract the obstacles and ground vehicle.
#

import cv2
import numpy as np
import copy

class Camera:
    def __init__(self, mp, cameraPort, cameraResolution, environmentDataFilePath):
        # Message pipe instance
        self.mp = mp

        # Folder to put intermediate processing pictures in
        self.environmentDataFilePath = environmentDataFilePath
        self.mp.out(['Camera'], 'Initializing Camera...')

        # Start camera feed
        self.cameraFeed = cv2.VideoCapture(cameraPort)
        if not self.cameraFeed.isOpened():
            raise Exception("Camera not initialized. Check camera port number.")

        # Set camera resolution
        self.cameraFeed.set(3, cameraResolution[1])
        self.cameraFeed.set(4, cameraResolution[0])
        self.mp.out(['Camera'], 'Camera is configured at (%d, %d)' % (cameraResolution[1], cameraResolution[0]))

    def captureFrame(self):
        # Grabs and returns a frame from the camera
        self.mp.out(['Camera'], 'Capturing Camera Frame...')
        return self.cameraFeed.read()[1]

class SimulatedCamera:
    def __init__(self, mp, gui, gvInstance, framePath, environmentFrameFilename, simCameraResolution, gvInitialCoords):
        # Message pipe and gvInstance handles
        self.mp = mp
        self.gui = gui
        self.gvInstance = gvInstance

        # Initializing sim camera variables
        mp.out(['SimCamera'], 'Initializing Simulated Camera...')
        self.framePath = framePath
        self.simCameraResolution = simCameraResolution

        # Reads the environment drawing picture
        self.environmentImage = cv2.imread(self.framePath + environmentFrameFilename)
        if self.environmentImage is None:
            raise Exception('Could not find environment image at %s' % (self.framePath + environmentFrameFilename))

        # Generates a simulated camera frame with the ground vehicle at its initial coordinates
        self.currentFrame = None
        self.drawNewFrame(gvInitialCoords)

    def drawNewFrame(self, gvcoords):
        # Draws a new picture with the ground vehicle in a new position
        self.mp.out(['SimCamera'], 'Capturing Simulated Camera Frame...')
        # New copy of environment image so image gets cleared between runs
        environmentImageCopy = copy.deepcopy(self.environmentImage.copy())
        # Draws the Ground Vehicle
        self.currentFrame = cv2.circle(environmentImageCopy, gvcoords[::-1], self.gui.gvPixelRadius, (255, 255, 255), -1)
        # self.currentFrame = environmentImageCopy

    def captureFrame(self):
        # Returns a camera frame as though it were taken with a physical camera
        self.drawNewFrame((int(self.gui.gvCoords[0]), int(self.gui.gvCoords[1])))
        return self.currentFrame

class Analyst:
    def __init__(self, mp, gui, squareDimensions, cameraResolution):
        # Message pipe instance
        self.mp = mp
        # Gui instance
        self.gui = gui
        # Interpreter variables
        mp.out(['Interpreter', 'Status'], 'Initializing Interpreter...')
        self.squareWidth = squareDimensions[0]  # Deprecated
        self.squareHeight = squareDimensions[1] # Deprecated
        self.xRes = cameraResolution[1]
        self.yRes = cameraResolution[0]

    def analyzeFrame(self, currentFrame):
        self.mp.out(['Interpreter'], 'Finding objects...')
        # Apply some filters to individual copies of the current frame
        currentFrame = cv2.morphologyEx(currentFrame, cv2.MORPH_OPEN, np.ones((7, 7), np.uint8))
        blurredFrame = cv2.bilateralFilter(currentFrame, 8, 90, 90)
        edgesFrame = cv2.Canny(blurredFrame, self.gui.envThreshold, self.gui.envThreshold*2)

        # Find contours of current frame
        _, frameContours, _ = cv2.findContours(edgesFrame, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        # Contour lists
        obstacleContours = []
        gvContours = []

        for contour in frameContours:
            contourArea = cv2.contourArea(contour)
            contourLines = cv2.approxPolyDP(contour, 0.03*cv2.arcLength(contour, True), True)

            # Detect Obstacles
            if len(contourLines) < 7 or len(contourLines) > 8:
                obstacleContours.append(contour)

            # Detect Ground Vehicle
            if len(contourLines) > 6 and len(contourLines) < 9 and contourArea > 100:
                gvContours.append(contour)

                # Calculate center of ground vehicle
                contourMoment = cv2.moments(contour)
                gvX1 = int(contourMoment['m10'] / contourMoment['m00']) + 1
                gvY1 = int(contourMoment['m01'] / contourMoment['m00']) + 1
                gvX0 = self.gui.travelHistory[-1][0][1]
                gvY0 = self.gui.travelHistory[-1][0][0]
                self.gui.gvCoords = (gvY1, gvX1)
                self.gui.gvHeading = np.arctan2(gvY1 - gvY0, gvX1 - gvX0)

        # Sends current frame to the main window
        self.gui.currentFrame = currentFrame
        # Sends the extracted ground vehicle to ground vehicle window
        self.gui.gvContours = gvContours
        # Sends the extracted obstacles to obstacle window
        self.gui.obstacleContours = obstacleContours
