#!python3

# ANGELS
# Senior Design Project
# Authors: Jason Carruth, Nicholas Hampton, Mark Travers
#
# Fall 2018
#
# This module has one class:
#   GUI - Handles all graphical interface needs. 
#       Handles most of the system's dataflow. (This item should be separated out in future versions for clarity)

import cv2
import numpy as np

class GUI:
    def __init__(self, mp, navigationWindowResolution, gvPixelRadius, gvInitialCoords, gvInitialHeading):
        # Message pipe instance
        self.mp = mp
        self.mp.out(['GUI', 'Status'], 'Opening windows...')
        ############################## Main navigation window ##################################
        self.navigationWindowResolution = navigationWindowResolution
        self.cameraDrawing = None
        cv2.namedWindow('User Navigation Control', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('User Navigation Control', self.navigationWindowResolution[1], self.navigationWindowResolution[0])
        ############################## Settings window #########################################
        # Trackbar variables
        self.systemActive = 1
        self.envThreshold = 80
        self.erosionPixels = 40
        self.gvPixelRadius = gvPixelRadius
        # PID Gains
        self.dkp = 0.060
        self.dki = 0.00090
        self.dkd = 0.30
        self.thetakp = 0.030
        self.thetaki = 0.00030
        self.thetakd = 0.000
        # Controller mixing gains
        self.dFactor = 0.5
        self.thetaFactor = 0.5
        self.kh = 20
        cv2.namedWindow('Settings', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Settings', self.navigationWindowResolution[1], self.navigationWindowResolution[0]*2)
        # Create trackbars
        # Computer Vision Parameters
        cv2.createTrackbar('Sys Active', 'Settings', self.systemActive, 1, self.systemActiveCallback)
        cv2.createTrackbar('Edge Detection', 'Settings', self.envThreshold, 200, self.envThresholdCallback)
        cv2.createTrackbar('Safety Buffer', 'Settings', self.erosionPixels, 80, self.erosionPixelsCallback)
        cv2.createTrackbar('GV Radius', 'Settings', self.gvPixelRadius, 100, self.gvPixelRadiusCallback)
        # PID Gains
        cv2.createTrackbar('D-kp', 'Settings', int(self.dkp*1000), 100, self.dkpCallback)
        cv2.createTrackbar('D-ki', 'Settings', int(self.dki*100000), 100, self.dkiCallback)
        cv2.createTrackbar('D-kd', 'Settings', int(self.dkd*100), 100, self.dkdCallback)
        cv2.createTrackbar('Theta-kp', 'Settings', int(self.thetakd*1000), 100, self.thetakpCallback)
        cv2.createTrackbar('Theta-ki', 'Settings', int(self.thetakd*100000), 100, self.thetakiCallback)
        cv2.createTrackbar('Theta-kd', 'Settings', int(self.thetakd*100), 100, self.thetakdCallback)
        # Controller mixing parameters
        cv2.createTrackbar('D %', 'Settings', int(self.dFactor*100), 100, self.dFactorCallback)
        cv2.createTrackbar('Theta %', 'Settings', int(self.thetaFactor*100), 100, self.thetaFactorCallback)
        cv2.createTrackbar('kh', 'Settings', self.kh, 127, self.khCallback)
        ############################## GV window ###############################################
        self.gvDrawing = None
        cv2.namedWindow('Ground Vehicle')
        cv2.resizeWindow('Ground Vehicle', self.navigationWindowResolution[1], self.navigationWindowResolution[0])
        ############################## Obstacle window #########################################
        self.obstacleDrawing = None
        cv2.namedWindow('Obstacle')
        cv2.resizeWindow('Obstacle', self.navigationWindowResolution[1], self.navigationWindowResolution[0])
        # Navigation Controls
        self._destCoords = (None, None)
        self._gvCoords = gvInitialCoords
        self.gvHeading = gvInitialHeading
        self.needNewPath = False
        self._gvTraveling = False
        # GV history: Position, Distance Error, Heading, Heading Error
        self.travelHistory = [[self._gvCoords, 0.0, self.gvHeading, 0.0]]
        # Drawing data
        self.pathNodeList = []
        self.currentFrame = None
        self.gvContours = []
        self.obstacleContours = []
        # Attach mouse callback
        cv2.setMouseCallback('User Navigation Control', self.mouseCallback)

    # Getters and Setters
    @property
    def destCoords(self):
        return(self._destCoords)
    @destCoords.setter
    def destCoords(self, destCoords):
        self._destCoords = destCoords
    @property
    def gvCoords(self):
        return(self._gvCoords)
    @gvCoords.setter
    def gvCoords(self, gvCoords):
        self._gvCoords = gvCoords
    @property
    def gvTraveling(self):
        return(self._gvTraveling)
    @gvTraveling.setter
    def gvTraveling(self, state):
        if state == True:
            self.travelHistory = [[self._gvCoords, 0.0, self.gvHeading, 0.0]]
        self._gvTraveling = state

    ######################## Trackbar callback functions ##########################
    # Global Parameters
    def systemActiveCallback(self, trackbarData):
        self.systemActive = trackbarData
        if self.systemActive == 0:
            self.mp.out(['GUI', 'Status'], 'System Stopped.')
        elif self.systemActive == 1:
            self.mp.out(['GUI', 'Status'], 'System Started.')
    # Computer Vision Parameters
    def envThresholdCallback(self, trackbarData):
        self.envThreshold = trackbarData
        self.mp.out(['GUI'], 'envThreshold = %d' % self.envThreshold)
    def erosionPixelsCallback(self, trackbarData):
        self.erosionPixels = trackbarData
        self.mp.out(['GUI'], 'erosionPixels = %d' % self.erosionPixels)
    def gvPixelRadiusCallback(self, trackbarData):
        self.gvPixelRadius = trackbarData
        self.mp.out(['GUI'], 'gvPixelRadius = %d' % self.gvPixelRadius)
    # PID Gains
    def dkpCallback(self, trackbarData):
        self.dkp = float(trackbarData)/1000.0
        self.mp.out(['GUI'], 'kp = %f' % self.dkp)
    def dkiCallback(self, trackbarData):
        self.dki = float(trackbarData)/100000.0
        self.mp.out(['GUI'], 'ki = %f' % self.dki)
    def dkdCallback(self, trackbarData):
        self.dkd = float(trackbarData)/100.0
        self.mp.out(['GUI'], 'kd = %f' % self.dkd)
    def thetakpCallback(self, trackbarData):
        self.thetakp = float(trackbarData)/1000.0
        self.mp.out(['GUI'], 'kp = %f' % self.thetakp)
    def thetakiCallback(self, trackbarData):
        self.thetaki = float(trackbarData)/100000.0
        self.mp.out(['GUI'], 'ki = %f' % self.thetaki)
    def thetakdCallback(self, trackbarData):
        self.thetakd = float(trackbarData)/100.0
        self.mp.out(['GUI'], 'kd = %f' % self.thetakd)
    # Controller Mixing Gains
    def dFactorCallback(self, trackbarData):
        self.dFactor = float(trackbarData/100.0)
        self.mp.out(['GUI'], 'D-Factor = %f' % self.dFactor)
    def thetaFactorCallback(self, trackbarData):
        self.thetaFactor = float(trackbarData/100.0)
        self.mp.out(['GUI'], 'Theta-Facotr = %f' % self.thetaFactor)
    def khCallback(self, trackbarData):
        self.kh = float(trackbarData)
        self.mp.out(['GUI'], 'kh = %f' % self.kh)

    # This function is called when mouse input is received
    def mouseCallback(self, mouseEvent, mouseX, mouseY, flags, param):
        # Checks if the left mouse button was pressed
        if mouseEvent == cv2.EVENT_LBUTTONDOWN:
            if self.obstacleDrawing[mouseY, mouseX] == 0:
                # Set the ground vehicle's destination
                self.destCoords = [mouseY, mouseX]
                # Request a path from the ground vehicle to the destination
                self.needNewPath = True
                # Stop the ground vehicle if it is currently traveling
                self.gvTraveling = False
            else:
                self.mp.out(['Pathing', 'Non-Fatal Error', 'GUI'], 'Invalid destination selected.')

    # Checks to see if any keys were pressed
    def captureUserKeys(self, timeout):
        userKey = cv2.waitKey(timeout)
        # Press 'z' to enter fullscreen
        if userKey == 122:
            self.mp.out(['GUI'], 'Entering full-screen...')
            cv2.setWindowProperty('User Navigation Control', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        # Press 'x' to exit fullscreen
        if userKey == 120:
            self.mp.out(['GUI'], 'Exiting full-screen...')
            cv2.namedWindow('User Navigation Control', cv2.WINDOW_NORMAL)
            cv2.resizeWindow('User Navigation Control', self.navigationWindowResolution[1], self.navigationWindowResolution[0])
        self.mp.out(['GUI'], 'GUI Refreshed.')
        return userKey

    # Draws a red target over the ground vehicle on the main window
    def drawGvOverlay(self):
        # Check to see if the ground vehicle is in the frame
        if self.gvCoords[0] == None or self.gvCoords[1] == None:
            self.mp.out(['GUI', 'Non-Fatal Error'], 'GV not found in current frame.')
        else:
            # Draw ground vehicle overlay on camera frame
            cv2.circle(self.cameraDrawing, self.gvCoords[::-1], self.gvPixelRadius, color=(0,0,255), thickness=4)
            cv2.circle(self.cameraDrawing, self.gvCoords[::-1], 5, color=(0,0,255), thickness=-1)

    # Draw aStar path nodes on the main window
    def drawPathOverlay(self):
        for pathNode in self.pathNodeList:
            nodeColor = (255,255,0)
            if pathNode.originFlag or pathNode.destinationFlag:
                nodeColor = (0,255,0)
            # Draw a small circle at the node (indices are reversed to convert from matrix notation)
            cv2.circle(self.cameraDrawing, tuple(pathNode.pixelCoords[::-1]), 2, color=nodeColor, thickness=-1)

    # Draw a line where the ground vehicle actually traveled
    def drawActualPathOverlay(self):
        for travelPoint in self.travelHistory:
            cv2.circle(self.cameraDrawing, tuple(travelPoint[0][::-1]), 2, color=(0,255,255), thickness=-1)

    # Draw the ground vehicle contour on the ground vehicle window
    def drawGv(self):
        # Allocate space for drawings
        self.gvDrawing = np.zeros(self.currentFrame.shape[0:2], np.uint8)
        for contour in self.gvContours:
            # Draw Ground Vehicle
            cv2.drawContours(self.gvDrawing, [contour], 0, (255), -1)

    # Draw the obstacle contours on the obstacle window
    def drawObstacles(self):
        # Allocate space for drawings
        self.obstacleDrawing = np.zeros(self.currentFrame.shape[0:2], np.uint8) * 255
        # Draw obstacles
        for contour in self.obstacleContours:
            # Draw walls
            cv2.drawContours(self.obstacleDrawing, [contour], 0, (0), -1)
            # Draw safety stripe around walls
            cv2.drawContours(self.obstacleDrawing, [contour], 0, (255), self.erosionPixels*2)

    # Construct each drawing (drawing order may be rearranged) and update the windows
    def updateWindows(self):
        # Update the 'User Navigation Control' drawing
        self.cameraDrawing = self.currentFrame
        self.drawPathOverlay()
        self.drawGvOverlay()
        self.drawActualPathOverlay()
        # Update the 'Ground Vehicle' drawing
        self.drawGv()
        # Update the 'Obstacle' drawing
        self.drawObstacles()

        # Update the windows with the new drawings
        cv2.imshow('User Navigation Control', self.cameraDrawing)
        cv2.imshow('Ground Vehicle', self.gvDrawing)
        cv2.imshow('Obstacle', self.obstacleDrawing)
        # cv2.imwrite('./frames/%s.png' % frameCounter, self.obstacleDrawing)
        self.mp.out(['GUI'], 'Windows updated.')

    # Performs any necessary tasks to stop the ground vehicle (should be removed in future versions)
    def reachDestination(self):
        self.gvTraveling = False
        self.mp.out(['Pilot', 'Status'], 'Reached Destination.')
