import numpy as np
import cv2
from PIL import Image
import time
import serial
import threading
import os.path

xRes = 640
yRes = 480

cap = cv2.VideoCapture(0) # find camera feed (port 0)
cap.set(4, xRes)          #set camera resolution
cap.set(3, yRes)          #set camera resolution

cv2.namedWindow('Frame', cv2.WINDOW_NORMAL)
cv2.resizeWindow('Frame', (xRes*2),(yRes*2))

path = r"C:\Users\ZEe\Desktop\maze" 

mazeFlag = True
pointFlag = True
greenPoint = True
redPoint = True
bluePoint = True
lBluePoint = True
selection = True
selectionFilter = True
routeFlag = True
gNext = True
rNext = True
bNext = True
lbNext = True
greenPrint = True
redPrint = True
bluePrint = True
lBluePrint = True

botCenter = (200,200)
frameCounter = 0

greenCoord = []
redCoord = []
blueCoord = []
lBlueCoord = []

squareWidth = 50
squareHeight = 60

######### Track bar setup ###############

def nothing1(x):
    pass

def nothing2(x):
    pass

def nothing3(x):
    pass

def nothing4(x):
    pass

cv2.namedWindow("Settings")

cv2.createTrackbar("maze", "Settings", 10, 100, nothing1)
cv2.createTrackbar("Find Robot", "Settings", 0, 300, nothing2)
#cv2.createTrackbar("Robot Orientation", "Settings", 0, 400, nothing3)
cv2.createTrackbar("pixelDistance", "Settings", 10, 80, nothing4)


######### Track bar setup END ###############

def findObject(thresh, img, frameObject):
    
    img = cv2.morphologyEx(img, cv2.MORPH_OPEN, np.ones((7,7),np.uint8))
    blur = cv2.bilateralFilter(img,8,90,90)
    edges = cv2.Canny(blur,thresh,thresh*2)
    
    if frameObject == 'maze':
        edges = cv2.dilate(edges, None, iterations=1)
        edges = cv2.erode(edges, None, iterations=1)
    
    drawing = np.zeros(img.shape,np.uint8)    #Image to draw the contours
    drawingBot = np.zeros(img.shape,np.uint8)    #Image to draw the contours
    
    _,contours,_ = cv2.findContours(edges,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
    for cnt in contours:
        
        area = cv2.contourArea(cnt)
        moments = cv2.moments(cnt) 
        approx = cv2.approxPolyDP(cnt,0.03*cv2.arcLength(cnt,True),True)#0.03 
        
        if len(approx)>8 or len(approx)<7 and frameObject == 'maze': #maze
            cv2.drawContours(drawing,[cnt],0,(255,255,255),-1)
            cv2.drawContours(drawing,[cnt],0,(0,0,0),pixelDistance*2)
            
        if len(approx)>6 and len(approx)<9 and frameObject == 'bot' and area > 1000: # robot
            cv2.drawContours(drawingBot,[cnt],0,(255,255,255),-1)

#        if len(approx)==3 and (frameObject == 'orientation'):
#            cv2.drawContours(drawing,[cnt],0,(255,255,255),-1)
            
    if frameObject == 'bot':  
        cv2.imwrite(frameObject + '.png',drawingBot)
        cv2.imshow(frameObject,drawingBot)
    if frameObject == 'maze': 
        cv2.circle(drawing,(squareWidth+25,squareWidth+25), int(pixelDistance/2), (255,255,255), -1) #green
        cv2.circle(drawing,(squareWidth+35,yRes-squareHeight-20), int(pixelDistance/2), (255,255,255), -1) #red
        cv2.circle(drawing,(xRes-squareHeight-25, squareWidth+20), int(pixelDistance/2), (255,255,255), -1) #blue
        cv2.circle(drawing,(xRes-squareHeight-25, yRes-squareHeight-20),int(pixelDistance/2), (255,255,255), -1)
        
        cv2.imwrite(frameObject + '.png',drawing)
        cv2.imshow(frameObject,drawing)
    

def saveBackBitMask(frameObject):    
      
    img = Image.open(path + '\\' + frameObject + ".png").convert('L')  # convert image to 8-bit grayscale
    xRes,yRes = img.size    
    data = np.array(img)
    filter(None, data)

    data[data < 250] = 1
    data[data >=250] = 0
    
    np.savetxt(frameObject + '.txt', data, fmt='%d')
 
    return data

def saveBackPic(frameObject):    
        global botCenter
        img = cv2.imread(path + '\\' + frameObject + ".png")
        edges = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
            
        _,contours,_ = cv2.findContours(edges,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
        
        for cnt in contours:
            area = cv2.contourArea(cnt)
            moments = cv2.moments(cnt)                          # Calculate moments
            if moments['m00']!=0:
                cx = int(moments['m10']/moments['m00'])         # cx = M10/M00
                cy = int(moments['m01']/moments['m00'])         # cy = M01/M00
            
                botCenter = cx,cy
                return cx,yRes-cy
            else:
                
                botCenter = 200,200
                return 200,200
        

def draw_circle(event,mouseX,mouseY,flags,param):
    
    global greenPoint 
    global redPoint
    global bluePoint
    global lBluePoint
    global greenCoord
    global blueCoord
    global redCoord
    global lBlueCoord
    global selection
    
    if event == cv2.EVENT_LBUTTONDOWN and selection == True:
        if (green[mouseY,mouseX] == 0): 
            print('green selected')
            greenCoord = saveBackPic('green')
            redPoint = False
            bluePoint = False
            lBluePoint = False
            selection = False
            
        elif (red[mouseY,mouseX] == 0):
            print('red selected')
            redCoord = saveBackPic('red')
            greenPoint = False
            bluePoint = False
            lBluePoint = False
            selection = False
            
        elif (blue[mouseY,mouseX] == 0):
            print('blue selected')
            blueCoord = saveBackPic('blue')
            greenPoint = False
            redPoint = False
            lBluePoint = False
            selection = False
            
        elif (lightBlue[mouseY,mouseX] == 0):
            print('light Blue selected')
            lBlueCoord = saveBackPic('lightBlue')
            greenPoint = False
            redPoint = False
            bluePoint = False
            selection = False
            
        else:
            print('pick a color!')
       
while True:
    _, frame = cap.read()
   
    ObstacleLayer = frame.copy()
    robotTrace = frame.copy()
    orientation = frame.copy()

    threshObstacles = cv2.getTrackbarPos("maze", "Settings")
    threshBot = cv2.getTrackbarPos("Find Robot", "Settings")
    #threshOrientation = cv2.getTrackbarPos("Robot Orientation", "Settings")
    pixelDistance = cv2.getTrackbarPos("pixelDistance", "Settings") #how many radius pixels away from obstacle edge do we want to keep final destination?
       
    if greenPoint == True:
        cv2.circle(frame,(squareWidth+25,squareWidth+25), int(pixelDistance/2), (0,255,0), -1) #green
    if redPoint == True:
        cv2.circle(frame,(squareWidth+35,yRes-squareHeight-20), int(pixelDistance/2), (0,0,255), -1) #red
    if bluePoint == True:
        cv2.circle(frame,(xRes-squareHeight-25, squareWidth+20), int(pixelDistance/2), (255,0,0), -1) #blue
    if lBluePoint == True:
        cv2.circle(frame,(xRes-squareHeight-25, yRes-squareHeight-20),int(pixelDistance/2), (255,255,0), -1) #light blue
    
    if mazeFlag == True:
        findObject(threshObstacles, ObstacleLayer, 'maze')
       
    if frameCounter%10 == 0:
        findObject(threshBot, robotTrace, 'bot')
        img = cv2.imread(path + '\\' + "bot.png")
        edges = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
            
        _,contours,_ = cv2.findContours(edges,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
        
        for cnt in contours:
            area = cv2.contourArea(cnt)
            moments = cv2.moments(cnt)                          # Calculate moments
            if moments['m00']!=0:
                x2 = int(moments['m10']/moments['m00'])         # cx = M10/M00
                y2 = int(moments['m01']/moments['m00'])         # cy = M01/M00
            else:
                x2 = int(xRes/2)
                y2 = int(yRes/2)

    cv2.circle(frame,(x2,y2), pixelDistance, (255, 0, 255), 2)
    cv2.circle(frame, (x2,y2), 2, (255, 0, 255), -1) 
    
    if cv2.waitKey(2) == 119 and mazeFlag == True: #press w to calibrate
        global mazeCoords
        global blue
        global green
        global red
        global lightBlue
        
        greenz = np.zeros((yRes,xRes), dtype=int)
        cv2.circle(greenz,(squareWidth+25,squareWidth+25),int(pixelDistance/2),(255,255,255),-1)
        cv2.imwrite('green.png',greenz)
        green = saveBackBitMask('green')
        
        redz = np.zeros((yRes,xRes), dtype=int)
        cv2.circle(redz,(squareWidth+35,yRes-squareHeight-20),int(pixelDistance/2),(255,255,255),-1)
        cv2.imwrite('red.png',redz)
        red = saveBackBitMask('red')
        
        bluez = np.zeros((yRes,xRes), dtype=int)
        cv2.circle(bluez,(xRes-squareHeight-25, squareWidth+20),int(pixelDistance/2),(255,255,255),-1)
        cv2.imwrite('blue.png',bluez)
        blue = saveBackBitMask('blue')
        
        lightBluez = np.zeros((yRes,xRes), dtype=int)
        cv2.circle(lightBluez,(xRes-squareHeight-25, yRes-squareHeight-20),int(pixelDistance/2),(255,255,255),-1)
        cv2.imwrite('lightBlue.png',lightBluez)
        lightBlue = saveBackBitMask('lightBlue')
        
        mazeCoords = saveBackBitMask('maze')
        mazeFlag = False
        print('maze calibrated')
        print('final destination calibrated')
               
    if selection == False and selectionFilter == True:
        #global "path variable"
        finalBotCoords = x2,yRes-y2
        routeFlag = False
        selectionFilter = False
        if lBluePoint == True:
            print(finalBotCoords)
            print(lBlueCoord)
            ################# mazeCoords, finalBotCoords and color coords are aready to send
            #################  insert path function here            
        if bluePoint == True:
            print(finalBotCoords)
            print(blueCoord)
            ################# mazeCoords, finalBotCoords and color coords are aready to send
            #################  insert path function here   
        if greenPoint == True:
            print(finalBotCoords)
            print(greenCoord)
            ################# mazeCoords, finalBotCoords and color coords are aready to send
            #################  insert path function here   
        if redPoint == True:
            print(finalBotCoords)
            print(redCoord)
            ################# mazeCoords, finalBotCoords and color coords are aready to send
            #################  insert path function here 
            
    #for cp in "path variable":
        #cv2.circle(frame, cp, 2, (255, 255, 0), 2) 
        
    if routeFlag == False:    
        if green[y2,x2] == 0 and gNext == True:
            gNext = False
            rNext = True
            bNext = True
            lbNext = True
            frameCounter = 0
            routeFlag = True
            redPoint = True
            bluePoint = True
            lBluePoint = True
            greenPoint = False
            selection = True
            selectionFilter = True
            greenPrint = False 
            print('Green Destination Reached!')
            
        elif red[y2,x2] == 0 and rNext == True:
            gNext = True
            rNext = False
            bNext = True
            lbNext = True
            frameCounter = 0
            routeFlag = True
            greenPoint = True
            bluePoint = True
            lBluePoint = True
            redPoint = False
            selection = True
            selectionFilter = True
            redPrint = False 
            print('Red Destination Reached!')
            
        elif blue[y2,x2] == 0 and bNext == True:
            gNext = True
            rNext = True
            bNext = False
            lbNext = True
            frameCounter = 0
            routeFlag = True
            greenPoint = True
            redPoint = True
            lBluePoint = True
            bluePoint = False
            selection = True
            selectionFilter = True
            bluePrint = False 
            print('Blue Destination Reached!')
            
        elif lightBlue[y2,x2] == 0 and lbNext == True:
            gNext = True
            rNext = True
            bNext = True
            lbNext = False
            frameCounter = 0
            routeFlag = True
            greenPoint = True
            redPoint = True
            bluePoint = True
            selection = True
            lBluePoint = False
            selectionFilter = True
            lBluePrint = False 
            print('Light Blue Destination Reached!')
            
        else:
            pass
    
    if greenPrint == False:
        cv2.putText(frame, "Green Destination Reached!", (30,int(yRes/2)),cv2.FONT_HERSHEY_TRIPLEX, 1.0, (0,0,0))
    if redPrint == False:
        cv2.putText(frame, "Red Destination Reached!", (30,int(yRes/2)),cv2.FONT_HERSHEY_TRIPLEX, 1.0, (0,0,0))
    if bluePrint == False:
        cv2.putText(frame, "Blue Destination Reached!", (30,int(yRes/2)),cv2.FONT_HERSHEY_TRIPLEX, 1.0, (0,0,0))
    if lBluePrint == False:
        cv2.putText(frame, "Light Blue Destination Reached!", (30,int(yRes/2)),cv2.FONT_HERSHEY_TRIPLEX, 1.0, (0,0,0))
    
    #print text on screen for 2 seconds
    if frameCounter > 40:
        greenPrint = True
        redPrint = True
        bluePrint = True
        lBluePrint = True
        
    if cv2.waitKey(2) == 122: #press z
        cv2.setWindowProperty('Frame', cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
        
    if cv2.waitKey(2) == 120: #press x
        cv2.namedWindow('Frame', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Frame', (xRes*2),(yRes*2))
       
    cv2.imshow('Frame',frame)
    cv2.setMouseCallback("Frame", draw_circle) 
    
    frameCounter +=1
    key = cv2.waitKey(1)
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()