% A Star Pathing Algorthim (Version 2)
% Jason Carruth
% Created: 9-18-2018
% (Version 2):
%     Version 2 splits the A-Star algorithm into two files: a pre-processing file (preProcess_aStar.m) for instantiating commonly used arrays, and a main file (aStarV2.m) for the A-Star algorithm. "nodeClass.m" remaind its own separate file, but was updated to support aStar Version 2. Version 1's nodeClass.m file was not saved prior to updating, so it will neeed to be rewritten to support aStar Version 1 (if needed).

% NOTES:
%   -  This file assumes a two-level environment (value of 0 for the floor and value of 1 for the obstacles).
%   -  "Screen Coordinate" means that the coordinate pair being used is referenced from the lower-left corner of the screen. That is, the first element (0,0) is the pixel in the lower-left corner of the camera screen.
%   -  "Matrix Coordinate" (or "MatrixIndices") means that the coordinate pair being used is referenced from the first element in a MATLAB-based matrix. That is, the first element (1,1) is the "top-left" element in the MATLAB array.
%   -  Since MATLAB inherently uses a different array indexing technique than Python (MATLAB starts at [1 1] whereas Python starts at [0 0]), any indices used for accessing arrays will need to be changed accordingly.
%   Related to the MATLAB environment:
%      - MATLAB uses "for" and "end" statements (for functions, while-loops, for-loops, etc.) to encapsulate the code (like brackets in the C language).
%      - Functions in MATLAB are employed by stating "function [output1,output2,...] = funcName(input1,input2,input3,...)"

% Variable Descriptions:
%     cue                   The "open" set.
%     cueEnd                Used to track the size of the pre-instantiated "cue" array.
%     currentNode           The node currently under examination.
%     empty                 The "filler" values of [0 0] for the "empty" locations in the cue and visited sets. Replace [0 0] with [math.inf math.inf] if Python has issues with [0 0]. Maybe even doing something like [! !] would work (if it's treated as a charater string and not as the ASCII values [33 33]), but I think sticking with numerical values (like [inf inf]) would be a safe thing to do.
%     Fcost                 The total distance thus far between the origin and destination. Fcost = Gcost+Hcost;
%     Gcost                 The distance traveled thus far to a node from the origin.
%     Gprice                A Gcost multiplier. Used to control the pathfinding functionality.
%     GVDes                 Ground vehicle destination. Nick will pass this in screen-coordinate form; that is, the (0,0) reference location is the bottom-left corner of the screen.
%     GVPos                 Ground vehicle position. Nick will pass this in screen-coordinate form; that is, the (0,0) reference location is the bottom-left corner of the screen.
%     Hcost                 The distance from a node to the destination.
%     Hprice                An Hcost multiplier. Used to control the pathfinding functionality.
%     index                 An index (just a counter).
%     indexLocation         The index location of the next potential currentNode.
%     mapVariance           The difference between the new and previous depthMaps (variance=new-old).
%     node                  The array contaning the environment information per node (each screen pixel is an object).
%     parent                The preceding node to a discovered node. The node that "found" (or updated) another node. This variable is stored with MATLAB-based array indices and may need to be changed to be implemented using Python's indexing rules---the indexing starting at (0,0) for Python instead of (1,1) for MATLAB. Essentially, just switch it to use the screen coordinates.
%     pathList              The array containing the discretized guidance locations (waypoints).
%     screenRes             Screen resolution. Nick ought to pass this in the form: (rows, columns) == (810,1080). I'm not sure if Nick knows to do this yet.
%     sparseMapVariance     A sparse matrix of mapVariance.
%     visited               The "closed" set.
%     visitedEnd            Used to track the size of the pre-instantiated "visited" array.
%     [row, col, value]     The output arguments of the function find(). "value" is really called "vector" in the find() function, but "value" seemed like a more appropriate name. See find() reference: https://www.mathworks.com/help/matlab/ref/find.html#budqvni-1


function [node,pathList] = ...        % The symbol "..." means that the code is continued on the next line (it helps with readability sometimes). "function [node,pathList] = aStarV2(screenRes,node,mapVariance,GVPos,GVDest)"
    aStarV2(screenRes,node,mapVariance,GVPos,GVDest)%,Gprice,Hprice);      % In a perfect world, memory allocation for the cue and visited sets will depend on Gprice and Hprice. Gprice and Hprice should probably be inputs anyway in order to allow the user to control how the path computation functions (accuracy vs. speed).
% Assumptions:
%     Nick is inputting GVPos,GVDest in screen coordinates (he confirmed this). mapVariance is a difference of two maps, so the type of coordinate system being used for it won't have an effect on its use.
%     Mark is accepting a screen coordinate pathList? This is assumed to be true for this file. Since Python indices start at (0,0), screen coordinates will need to be used anyway. If Mark wants to have matrix coordinates (MATLAB-based matrix indices) outputted instead, then replace the two instances of "pathList(index,:) = node(parent(1),parent(2)).ScreenCoordinate" with "pathList(index,:) = node(parent(1),parent(2)).MatrixIndices" in the generatePathList() function as well as the instance at the end of the main ("aStarV2") function.


% tic               % "tic" and "toc" measure the time spent between these names. This is for testing only, but I left this in here as a reminder to check the time the program takes to run. To say explicitly, don't write this into the final Python code, it's just something to think about and consider.

% Check for initial format errors in the input (I would like to delete this function if we can ensure "clean" data input to aStar.m):
err = checkErrors(screenRes,mapVariance,GVPos,GVDest);%,node);             % All inputs to the aStar() function ought to be checked, however, since "node" is an object, this was going to be a pain. So, I left "node" out intentionally. If all of the inputs are always entered into aStar.m correctly anyway, then this shouldn't matter and the error checking function should be deleted.
if(err)
    error('Program Terminated.');                       % error() may not work in Python. Use whatever error() function equivalent or just print a message to the screen and quit the program.
end
% It would save on time if we didn't need to check for proper inputs all the
% time (every iteration of the ANGELS code). If we can be sure that the input will always be of a certain type
% and format, then this error-checking function should be deleted.

% We'll need to make sure Nick doesn't already do this in his code:
if(GVPos == GVDest)
    pathList = GVDest;  % All of these variables are in "screen coordinate" form right now. If Mark wants the pathList to be in matrix index format, run the following code prior to returning the aStar() function: pathList = screen2matrix(screenRes,GVDest);
    return
end

% Set new obstacle locations and unset old obstacle locations:
[row,col,value] = find(mapVariance);    % I don't know for sure, but this may take a while for large arrays (either because of a large screen resolution, or because the environment has changed so much that the varience in depthMaps results in a large nonzero array; or both). Check the time it takes.
sparseMapVariance = [row,col,value];    % sparseMapVariance is a sparse matrix of the mapVariance.
for index = 1:length(value)             % Positive values indicate "added" obstacles (where the obstacles moved to). Negative values indicate "vacated" obstacles (where the obstacles moved from).
    if(sparseMapVariance(index,3)==1)
        node(sparseMapVariance(index,1),sparseMapVariance(index,2)) = setObstacle(node(sparseMapVariance(index,1),sparseMapVariance(index,2)));
    elseif(sparseMapVariance(index,3)==-1) 
        node(sparseMapVariance(index,1),sparseMapVariance(index,2)) = unsetObstacle(node(sparseMapVariance(index,1),sparseMapVariance(index,2)));
    end
end

% Initializations:
GVPos = screen2matrix(screenRes,GVPos);   % Our convention was determined to be (0,0) at the lower-left corner of the screen. MATLAB indices, however, begin at (1,1) at the top-left corner of the matrix.
GVDest = screen2matrix(screenRes,GVDest); % This function assumes that Nick inputs screen coordinates like he said he would. After talking with Nick, he confirmed that he will, indeed, be inputting screen coordinates.
node(GVPos(1),GVPos(2)).Origin = true;
node(GVDest(1),GVDest(2)).Destination = true;
% I used [0 0] for empty values in the cue and visited sets since matrix indices start at [1 1] and are always greater than zero. For Python, however, I am guessing that different values will need to be used. If this is an issue, consider using infinity instead by importing the "math" library (use math.inf).
cue = zeros(screenRes(1)*screenRes(2),2);       % Memory allocation will depend on Gprice and Hprice. cue = allocateMemory(Gprice,Hprice,screenRes). For higher ratios of Gprice to Hprice (Gprice/Hprice), more memory will be needed since more nodes will be checked during the program (a path will try to stay as close to the origin as possible). Reducing memory allocation may be trivial in this case; however, since aStar() gets called frequently, aStar() should be as fast as possible! We'll consider this need appropriatly.
visited = zeros(screenRes(1)*screenRes(2),2);   % "visited" is used to track all of the "affected" nodes in the "node" array for every iteration of the ANGELS program (every time Computer Vision calls the aStar() function). visited and cue (or what's remaining in the cue) will be used to reset their flagging values (they're used to reset all of the nodes' values; this is a better option than reseting ALL of the nodes to their original values, even ones that hadn't been changed).
cueEnd = 0;         % cueEnd and visitedEnd are used to keep track of how many entries are in the cue and visited sets. The alternitive is to dynamically allocate entries using concatination, but this is computationally expensive.
visitedEnd = 0;
Gprice = 10;
Hprice = 10;        % I think Gprice and Hprice should be inputs to aStar() for path-computation control, which themselves would be set by an optimization function that is dependant on the environment's circumstance, but this is just a preference and an upgrade and is not needed. Possibility: More obstacles require more processing, so the less computations the better; so, use higher Hprice. For fewer obstacles, a more direct route will inevitably result; so, less computation is needed; so, more computation can be afforded; so, use a higher Gprice.
empty = [0 0];      % Set this to [inf inf] if there are issues with [0 0] being unavailable in Python (I suspect this will be the case).

% Add the start-node to the cue (the open set):
Gcost = 0;              % It may be good to instantiate this Gcost outside of the main while-loop. The less variables that need to be re-created in memory, the better.
Hcost = floor(Hprice*sqrt((GVDest(2)-GVPos(2))^2+(GVDest(1)-GVPos(1))^2));  % Many A-Star algorithms (like the ones seen in videos) seem to calculate Hcost by using the lowest number of "moves" (diagonal, vertical, and horzontal motions between nodes) to get to the destination; however, I take a more direct approach by just calculating the graphical distance between nodes and truncating the result. As long as the same system is used for all nodes, the difference in these methods shouldn't matter.
Fcost = Gcost+Hcost;    % Whether I use Fcost soon or not, I instantiate it so that it's not repeatedly intantiated in the while-loop.

node(GVPos(1),GVPos(2)).Gcost = Gcost;
node(GVPos(1),GVPos(2)).Hcost = Hcost;
node(GVPos(1),GVPos(2)).Fcost = Fcost;%setFcost(node(GVPos(1),GVPos(2)),Gcost);          % It may be faster to just add Gcost and Hcost into Fcost and set Fcost directly as opposed to using the setFcost() method. The setFcost() method, however, is proper if the properties of the nodeClass are set to private. I don't think the properties need to be private, though, assuming the input information I receive is "clean" and properly formatted all the time.
node(GVPos(1),GVPos(2)).Parent= GVPos;  % The final parent node is distinguished from the others by having a parent node of itself.

cueEnd = cueEnd+1;                      % Increment the open-set "size".
cue(cueEnd,:) = GVPos;
node(GVPos(1),GVPos(2)) = setCue(node(GVPos(1),GVPos(2)));

currentNode = nodeClass();              % Instantiate currentNode so that it isn't re-instantiated every iteration of the below while-loop.

% Begin finding the pathList:
while(~isequal(cue(1,:),empty))         % While the cue is not empty, find the path (if it exists). The path, if existing, ought to be found before the cue is empty.
% I left the following information for Mark to read for a better understanding 
% of why Gprice and Hprice should be inputs to aStar() (ANGELS global variables). 
% If Mark still decides to leave Gprice and Hprice hard-coded within aStar.m, then that's fine too:
    % Even if I find the destination, I would still want to check for better
    % available paths, and I'd do that by continuing the above while-loop until 
    % the cue is empty, then comparing the available paths---storing these 
    % paths separately for later comparison (assuming that aStar.m doesn't already 
    % find the shortest path initially, which would be dependant on the Gprice and Hprice). aStar() is set up to use 
    % Gprice and Hprice---as of now, these are both constant values of 10, but they 
    % could be implemented as dynamically changing with respect to the 
    % environment topography (for a new topography, there'll be a new Gprice and Hprice). Using dynamically changing Gprice and Hprice 
    % values, there would be no need to check for better paths as these 
    % would intentionally be set to optimal values through an optimization 
    % function---where they would remain their constant optimal values (after returning from the optimization function that sets them) for a 
    % particular topography throughout aStar() resulting in a single path.

    % Set currentNode to the lowest Fcost value in the cue:
    indexLocation = 1;
    currentNode = node(cue(indexLocation,1),cue(indexLocation,2)); % Start with the first node in the open set and index from there to find the lowest Fcost.
    index = 2;
    while (index<=length(cue(:,1)) && ~isequal(cue(index,:),empty))
        if(node(cue(index, 1),cue(index, 2)).Fcost<currentNode.Fcost)
            currentNode = node(cue(index,1),cue(index,2));
            indexLocation = index;
        elseif(node(cue(index,1),cue(index,2)).Fcost==currentNode.Fcost) % If Fcost values equal, set currentNode to lowest Hcost value
            if(node(cue(index,1),cue(index,2)).Hcost<currentNode.Hcost)
                currentNode = node(cue(index,1),cue(index,2));
                indexLocation = index;
%             elseif(cue(index, H)==currentNode(H))     % Determine a tie breaker between equal Hcosts. It may not be worth the computation time.
                    %Will determining the best node take more time than just
                    %picking one and testing it?? Find worst case scenario.
            end
        end
        index = index+1;
    end
    
    % Add currentNode to the closed set. 
    visitedEnd = visitedEnd+1;
    visited(visitedEnd,:) = currentNode.MatrixIndices;
    %For the following code, I technically don't need to set the visited flag. I only need to remove the cue flag once nodes have been visited.
    %Since setting the visited flag is only one more little bit of code, I decided to leave setVisited() in the code for 
    %the sake of formality and readability. However, there may be several internal computational operations taking place to perform this function, 
    %so it would be good not to have it in the code in order to reduce as much time as we can.
    node(currentNode.MatrixIndices(1),currentNode.MatrixIndices(2)) = setVisited(node(currentNode.MatrixIndices(1),currentNode.MatrixIndices(2))); % When putting nodes in the cue (this is done when finding neighboring nodes) there is a condition for checking that the Null flag is true (and hence the Visited flag is false).

    % Remove currentNode from open set:
    cue(indexLocation,:) = cue(cueEnd,:);
    cue(cueEnd,:) = empty;
    cueEnd = cueEnd-1;

    % Find neighboring (surrounding) nodes to the currentNode:
    for row = -1:1      % These for-loop indices prgress in a left-to-right top-to-bottom fassion (like reading a book).
        for col = -1:1
            currentPos = [currentNode.MatrixIndices(1)+row,currentNode.MatrixIndices(2)+col];   % Collect the postion of the potential neighbor.
            if((row==0&&col==0)||currentPos(1)>screenRes(1)||currentPos(1)<1||currentPos(2)>screenRes(2)||currentPos(2)<1||...  % Determine if the potential neighbor is worth examining (not out-of-bounds, etc.).
                    node(currentPos(1),currentPos(2)).Obstacle)
                continue
            end
            Gcost = currentNode.Gcost+Gprice*(1+.4*~xor(row,col));  % A value of 1 is for vertical and horizontal movements, and 1.4 is for diagonal movements. Gprice then scales up the value.
            Hcost = floor(Hprice*sqrt((GVDest(2)-currentPos(2))^2+(GVDest(1)-currentPos(1))^2));
            Fcost = Gcost+Hcost;
            if(Fcost<node(currentPos(1),currentPos(2)).Fcost)       % If a node has a lower Fcost than it had in the past, update the node to have the lower values (Gcost, Hcost, and Fcost) and set set the parent node accordingly.
                if(Gcost<node(currentPos(1),currentPos(2)).Gcost)
                    node(currentPos(1),currentPos(2)).Gcost = Gcost;
                end
                if(Hcost<node(currentPos(1),currentPos(2)).Hcost)
                    node(currentPos(1),currentPos(2)).Hcost = Hcost;
                end
                node(currentPos(1),currentPos(2)).Fcost = Fcost;%setFcost(node(currentPos(1),currentPos(2)),Gcost);      % It may be faster to just set Fcost manually [node().Fcost = Fcost]. That is if the properties are allowed to be public.
                node(currentPos(1),currentPos(2)).Parent = currentNode.MatrixIndices;
            end
            
            % Check for the destination:
            if(isequal(currentPos,GVDest))
                pathList = generatePathList(node,GVPos,GVDest,visitedEnd);
                node = resetNodes(node,cue,cueEnd,visited,visitedEnd);
%                 toc       % This is where aStar() would finish if a path was found.
                return
            end
            
            % If the node is not already in the cue (has not been "found"), and it has not been visited, then put it in the cue.
            if(node(currentPos(1),currentPos(2)).Null)
                cueEnd = cueEnd+1;      % Increment the open set "size".
                cue(cueEnd,:) = currentPos;
                node(currentPos(1),currentPos(2)) = setCue(node(currentPos(1),currentPos(2)));
            end
        end
    end   
end

if(isequal(cue(1,:),empty))             % If the path is not found and the cue is empty, return GVPos as the pathList. The vehicle will, consequently, not move from its initial position.
    pathList = node(GVPos(1),GVPos(2)).ScreenCoordinate;    % If Mark wants matrix indices instead of screen coordinates, then replace this line of code with "pathList = node(GVPos(1),GVPos(2)).MatrixIndices"
    fprintf(2,'\n\nUnreachable Destination.\nProgram Terminated.\n\n');
%   toc       % This is where aStar() would finish if a path was not found.
    return
end

end

function value = checkErrors(screenRes,mapVariance,GVPos,GVDest)%,node)   All inputs to the aStar() function ought to be checked, however, since "node" is an object, this was going to be a pain. So, I left "node" out intentionally. If all of the inputs are always entered correctly anyway, then this should matter, and the error checking function should be deleted.
    % I personally don't think this is really the best way, or even a complete way (not considering all possible problems the inputs could have), to go about this.
    % I think the aStar() function inputs ought to be ensured to be inputted correctly, and that this function be deleted.

    value = false;
    str1='';str2='';str3='';str4='';
    if(isempty(mapVariance)||isempty(GVPos)||isempty(GVDest)||isempty(screenRes))
        if(isempty(mapVariance))
            str1 = "depthMap "; end
        if(isempty(GVPos))
            str2 = "GVPos "; end
        if(isempty(GVDest))
            str3 = "GVDest"; end
        if(isempty(screenRes))
            str3 = "screenRes"; end
        args = strcat(str1,str2,str3,str4,str5);
        fprintf(2,"\n\naStar.m failed.\n" + ...
              "The following function inputs are empty:\n" + ...
              "%s\n\n", args);
        value = true;
    end
    str1='';str2='';str3='';str4='';
    if(~isreal(mapVariance)||~isreal(GVPos)||~isreal(GVDest)||~isreal(screenRes))
        if(~isreal(mapVariance))
            str1 = "depthMap "; end
        if(~isreal(GVPos))
            str2 = "GVPos "; end
        if(~isreal(GVDest))
            str3 = "GVDest"; end
        if(~isreal(screenRes))
            str4 = "screenRes"; end
        args = strcat(str1,str2,str3,str4);
        fprintf(2,"\n\naStar.m failed.\n" + ...
              "The following function inputs contain nonreal elements:\n" + ...
              "%s\n\n", args);
        value = true;
    end
    str1='';str2='';str3='';str4='';
    if(~isnumeric(mapVariance)||~isnumeric(GVPos)||~isnumeric(GVDest)||~isnumeric(screenRes))
        if(~isnumeric(mapVariance))
            str1 = "depthMap "; end
        if(~isnumeric(GVPos))
            str2 = "GVPos "; end
        if(~isnumeric(GVDest))
            str3 = "GVDest"; end
        if(~isnumeric(screenRes))
            str4 = "screenRes"; end
        args = strcat(str1,str2,str3,str4);
        fprintf(2,"\n\naStar.m failed.\n" + ...
              "The following function inputs contain non-numeric elements:\n" + ...
              "%s\n\n", args);
        value = true;
    end
end

function indices = screen2matrix(screenRes,position) % This function converts a screen-coordinate pair into a MATLAB-based matrix-coordinate pair. Make sure this still works for a Python-based matrix.
    indices(1) = screenRes(1) - position(2);
    indices(2) = position(1) + 1;
end

function pathList = generatePathList(node,GVPos,GVDest,visitedEnd)
%The pathList will be at most visitedEnd+1 elements long since
%nothing has been evaluated or "examined" apart from what has been visited. Therefore,
%there's no sense allocating all of the nodes in the node array to the pathList.
% Hence, pathList = zeros(visitedEnd+1,2);
    pathList = zeros(visitedEnd+1,2);   % The "+1" is for the destination having not been marked as visited since it was only just now "found".
    parent = GVDest;
    index = length(pathList(:,1));      % Since I'm back-tracing the parent coordinates, I need to start from the end of the pathList (GVDest) and work towards the start node (GVPos), which is to be at the front of the pathList.
    while(~isequal(parent,GVPos))
        pathList(index,:) = node(parent(1),parent(2)).ScreenCoordinate;
        parent = node(parent(1),parent(2)).Parent;
        index = index-1;
    end
    pathList(index,:) = node(parent(1),parent(2)).ScreenCoordinate;     % Put in the final coordinate of GVPos, which didn't get evaluated in the while-loop because GVPos caused it to terminate.
    index = index-1;    % Set the ending location for the matrix-trimming function below.
    if(index>0)         % Trim the pathList (if it needs to be trimmed) to only contain the pathList coordinates and not extra empty ([0 0]) coordinates.
        pathList(1:index,:) = [];    % "[]" will delete the matrix element.
    end
end

function node = resetNodes(node,cue,cueEnd,visited,visitedEnd)
    for index = 1:visitedEnd
        node(visited(index,1),visited(index,2)) = resetNode(node(visited(index,1),visited(index,2)));
    end
    for index = 1:cueEnd
        node(cue(index,1),cue(index,2)) = resetNode(node(cue(index,1),cue(index,2)));
    end
end










