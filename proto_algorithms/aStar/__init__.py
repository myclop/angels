# Go to the following website to understand __init__.py and its use:
# http://mikegrouchy.com/blog/2012/05/be-pythonic-__init__py.html
# -------------------------------
# package/
#     __init__.py
#     file.py
#     file2.py
#     file3.py
#     subpackage/
#         __init__.py
#         submodule1.py
#         submodule2.py
# -------------------------------
# "As you can see in the structure above the inclusion of the __init__.py file in a directory (package/) indicates to the Python interpreter that the directory should be treated like a Python package."
# To understand better: the "package/" directory would be shown as "\package" at the end of a directory address. E.g. C:\Users\Jason\Desktop\package

# An example of a directory address looks like the following:
# C:\Users\Jason\Desktop\Python\Learning Python
# where "Learning Python" is the directory containing the .py files. If I wanted to treat "Learning Python" as a package---meaning, if I want the Python interpreter to treat the "Learning Python" directory as a package---then I would include a file named __init__.py in the "Learning Python" directory.
# An interpreter interprets a file. The file extention of the file is used to tell the interpreter---which is just another program (or file to be run)---that the file is written in such a way (in a format) that can be read by the inerpreter (or in a format that the interpreter can rely on when searching for data in specific locations in the file). For example, if I write a file in Notepad++ and give it my own extension of .thisismyextension (a rediculous extension, I know, but it gets the pint across), then I could write a MATLAB program (or a C program, or a Python program, or any other language) that opens my file and expects to find certain information (like a two digit integer number on some row in the file starting exactly at column 25).
# More information on interpreters at:
# https://www.techopedia.com/definition/7793/interpreter
