#!python3
# "#!python3" tells the computer what version of python to use when the file is run (e.g. when the I double click on the file in the windows explorer, or the desktop).

# Re-call this file when:
# - the screen resolution changes

import numpy as np
import time as tm   # use this for teseting how long code takes to run.

# function [depthMap,screenRes,node] = preProcess_aStar(screenRes)   # the only input should be screenSize. depthMap shouldn't be known yet since the main system loop hasn't started yet, but depthMap is instantiated here to have an intial depthMap of zeros to be used in the comparison (subtraction) between the old and the new depthMaps.
def preProcess_aStar(screenRes):    # screenRes == [810,1080]
    depthMap = np.zeros([screenRes[0],screenRes[1]],dtype=int,order='C') #[rows, columns]

    # Allocate Memory for Arrays
        # Consider only allocating an eigth of the pixels, and add more memory as needed to increase efficiency. Allocate in chunks [1/8, 1/4, 3/8, 1/2, 5/8, 3/4, 7/8, 1]. Create a function to allocate this memory.
        # Memory allocation would be dependant upon the Gprice and Hprice as the resulting computation will use up more (higher Gprice) or less (higher Hprice) of the total memory. To allocate memory automatically, write a function that takes the Gprice and Hprice as inputs and returns an allotment of memory.
        # Test the efficiency of the aStar() program by tracking the total number nodes that made it into the cue (and visited) array. Find the cuePointer (and checkedPointer) maximum in the worst case senario (and other senarios).

    # node(screenRes[0],screenRes[1]) = nodeClass()  # this takes about 4 seconds to do in MATLAB.
    for row in screenRes[0]
        for col in screenRes[1]
#             I may not need the following, but it is nice to have i guess.
#             node(row,col).Number = screenRes(2)*(screenRes(1)-row)+col
            node[row][col] = NodeClass()    # It would be nice to prepopulate a matrix full of NodeClass objects all at once outside of the for-loop, but whatever works. 
            node[row][col].ScreenCoordinate = [col-1,screenRes[0]-row]
            node[row][col].MatrixIndices = [row,col]
            

    return [depthMap, node];

    # update depthMap by depthMap_new-depthMap_old. Positive values are added obstacles, negative values are vacated obstacles. for multi-level environment, assign subtraction result values for each level.
    # updateDepthMap()
    #   mapVariance = depthMapOld-depthMapNew

    # depthMap = preProcess_aStar(screenSize)
    # Loop
    # depthMap = CVmapReturn - depthMap
    # aStar(depthMap)





    # Functions of interest:
    # reshape
    # find
    # sparse
    # spy     this shows sparse visually
    # nonzeros

