% Pre-Processing for A Star (for A-Star Version 2)
% Jason Carruth
% Created: 9-18-2018

% NOTES:
%   -  This file pre-initializes the "depthMap" and "node" arrays to be used in the
%       main ANGELS code loop so that they do not need to be re-created on every
%       iteration of the main code loop.
%   -  Since Python starts its array indexing at (0,0), as opposed to MATLAB's (1,1) starting point, the "MatrixIndices" variable may
%       not need to be used at all. Instead, use "screenCoordinate" values.
%   -  Re-call this file when the screen resolution changes.

% Variable Definitions:
%     depthMap            An 810 by 1080 array (the defined screen resolution) containing the binary values of the environment (1's are obstacles, ans 0's are drivable space).
%     node                An 810 by 1080 array (the defined screen resolution) containing the pixels of the screen as objects.
%     screenRes           A two-dimensional array of the screen resolution (810,1080) = (rows,cols). Within the proper context (being that screen resolution is not meant to be changed), I think screenRes would appropriately be a tuple data-type in Python; however, if (for whatever reason) we decide to change the screen resolution, it may be a good idea just to make screenRes a list instead (which may not even matter anyway if the resolution is hardcoded into the program's files).

% NOTE: Just as a reminder, all matrices should NOT be implemented as a tuple (if I
% understand correctly) because the values in these matrices will need to
% change throughout the ANGELS program (specifically in aStar.m).

function [depthMap,node] = preProcess_aStar(screenRes)

depthMap = zeros(screenRes); %(rows, columns)

% Initialize "node" and fill each object (each pixel) with its corresponding
% screen and MATLAB-based matrix coordinates.
node(screenRes(1),screenRes(2)) = nodeClass();  % This takes about 4 seconds to do in MATLAB.
    for row = 1:screenRes(1)
        for col = 1:screenRes(2)
%             We won't need the Number (it never gets used), but it is nice to have I guess.
%             node(row,col).Number = screenRes(2)*(screenRes(1)-row)+col;
            node(row,col).ScreenCoordinate = [col-1,screenRes(1)-row];
            node(row,col).MatrixIndices = [row,col];
        end
    end
end


% After preProcess_aStar.m runs:
% In the main ANGLES loop (after preProcess_aStar.m runs) update depthMap 
% by doing mapVariance = depthMap_new - depthMap_old, and input mapVariance into aStar(). Positive values are 
% added obstacles (locations where obstacles moved to), negative values are 
% vacated obstacles (locations where obstacles moved from).
%       function mapVariance = updateDepthMap(depthMapOld, depthMapNew)
%           mapVariance = depthMapOld-depthMapNew;
%       end


% Rough order of file execution (as relevant for aStar() operation):
%
% depthMap_old = preProcess_aStar(screenRes);
% begin ANGELS loop
% {
%     (other code)...
%     depthMap_new = computerVisionCode(inputs)
%     mapVariance = depthMap_new - depthMap_old;
%     depthMap_old = depthMap_new;
%     aStar(mapVariance);
%     (other code)...
% }




