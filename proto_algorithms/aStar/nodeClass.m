% Classes for A Star
% Jason Carruth
% Created: 9-18-2018

% NOTES:
%   -  Since Python starts its array indexing at (0,0), as opposed to MATLAB's (1,1) starting point, the "MatrixIndices" variable may
%       not need to be used at all. Instead, use "screenCoordinate" values for everything.

% Variable Definitions:
%     Cue                   A flag denoting nodes in the "open" set---nodes that have been found, but not examined yet (not set to the currentNode yet).
%     Destination           A flag denoting if the location is (or isn't) the destination.
%     Fcost                 The total path distance (from origin to destination) that has been currently dicovered.
%     Gcost                 The distance from the origin to the currentNode that has been currently dicovered.
%     Hcost                 The distance from the currentNode to the dstination that has been currently dicovered.
%     MatrixIndices         The MATLAB-based indices of a pixel's matrix coordinate location.
%     Null                  A flag denoting nodes in the "null" (empty) set.
%     Number                The designation, name, or alias of a node.
%     Obstacle              A flag denoting nodes that are obstacles.
%     Origin                A flag denoting if the location is (or isn't) the origin.
%     Parent                The coordinate of the parent node---the node that "found" (or updated) another node. Parent is stored with the MATLAB-based matrix indices. Switch this to properly implement Python (essentially, just switch it to use the screen coordinates).
%     ScreenCoordinate      The indices of a pixel's coordinate location as seen on the camera screen as defined by our team's referencing convention---the location (0,0) is at the bottom-left corner of the screen (implementing the first cartesian quadrant).
%     Visited               A flag denoting nodes in the "closed" set---nodes that have not only been found, but have also been "examined" (they have been set to the currentNode).

classdef nodeClass
    
    % The following are the default values for the properties:
    properties (GetAccess = public, SetAccess = public)         % For simplicity, and ease of acces, I set the properties to public. I imagine good coding practices would demand these to be private, but I'm done trying to figure that out.
%         Number = inf;             % Number is not being used, but it would be a formality to have it. To say explicitly, don't include this into the Python code (I'm simply just showing that it'd be something that pertains to these node objects for the sake of understanding their identity).
        ScreenCoordinate = [];      % These would be the proper coordinates to use in Python for pretty much every access to an array element.
        MatrixIndices = [];         % The MATLAB-based indices of the coordinate.
        Origin = false;
        Destination = false;
        Null = true;                % Null (empty) set.
        Cue = false;                % Open set. the remaining cue needs to be reset when aStar returns.
        Visited = false;            % Closed set. I'll need this to unset all of the visited and remaining cue nodes
        Obstacle = false;           % Restricted set.
        Gcost = inf;
        Hcost = inf;
        Fcost = inf;
        Parent = [];                % This coordinate is stored as MATLAB-based array indices. For Python, it ought to be implemented using "screenCoordinate" values.
    end
    
    
    
    methods
%       Constructor (use the default values as seen above for Python implementation):
        function obj = nodeClass()
        end
        
        function obj = resetNode(obj)
            % The Number (that would be in this line of code) is removed.
            obj.Origin = false;
            obj.Destination = false;
            obj.Null = true;
            obj.Cue = false;
            obj.Visited = false;
            % The Obstacle (that would be in this line of code) is removed because it doesn't need to be reset since it had always been false (being not an obstacle). If obstacle had been true, then none of the properties would have been affected, and the node would not need to be reset (unless the node becomes no longer an obstacle).
            obj.Gcost = inf;
            obj.Hcost = inf;
            obj.Fcost = inf;
            obj.Parent = [];
        end
        
% As is seen in the comments for each of the below methods, I directly set 
% the properties to true or false instead of negating the called property values. 
% I would imagine that the latter method (negating the called property values) 
% is better coding practice---being that these values aren't just hard-coded 
% as true or false---but for the sake of speed, I chose set these properties directly.
% I show these so that Mark can determine if this line of reasoning is correct and
% implement them accordingly---to sound all fancy-like ;-).

%         function obj = setFcost(obj,Gcost)  % I should set Fcost directly to save time.
%             obj.Fcost = obj.Hcost + Gcost;
%         end
        
        %Nodes should not be placed back into the cue once they're in the
        %visited set unless I write a function that 
        %finds multiple paths and optimally compares the available paths---in aStar.m, setting the cue flag will only operate on nodes that 
        %have false visited flag values (the aStar.m code only calls the setCue() function under conditions where the visited flag is false).
        function obj = setCue(obj)
            obj.Cue = true;
            obj.Null = false;%~obj.Cue;
        end
        
        % setVisited should only operate on nodes that have been in the cue,
        % meaning the Null flag will always have been false. So, "obj.Null = false" is not included in this function.
        function obj = setVisited(obj)
            obj.Visited = true;
            obj.Cue = false;%~obj.Visited;
        end
        
        function obj = setObstacle(obj)
            obj.Obstacle = true;
            obj.Null = false;%~obj.Obstacle;
        end
        
        
        % If a node is unset from an obstacle (it's no longer an obstacle),
        % then we need to check if the node's location (being available for origin/destination choosing by the user)
        % was chosen to be the origin or destination (unlikely as it is, it's possible).
        % Since the origin and destination get set after the obstacles are
        % updated, there's no need to set them in the unsetObstacle() method---I write this because 
        % I was trying to make sure everything that got affected by these
        % methods got accounted for and set appropriatly within the methods; since
        % the origin and destination get set externally to the unsetObstacle()
        % method anyway (and that the code sequentially comes after the unsetObstacle() 
        % method), I did not include them in the unsetObstacle() method to be set redundantly.
        function obj = unsetObstacle(obj)
            obj.Obstacle = false;
            obj.Null = true;%~obj.Obstacle;
        end

        
        
% I would have implemented the following code in ANGELS, but because of the 
% way aStar() runs, they would never get used---being that origin 
% and destination get re-instantiated every iteration of the ANGELS code.
% If origin and destination were global variables (instantiated outside
% of aStar.m), then these methods would need to be implemented. Simply put,
% these are here in case Mark asks why I'm not reseting the origin and 
% destination. They naturally get reset.
          
%         function obj = unsetOrigin(obj)
%             obj = setNull(obj);
%             obj.Origin = false;
%         end

%         function obj = unsetDest(obj)
%             obj = setNull(obj);
%             obj.Destination = false;
%         end

    end
    
end