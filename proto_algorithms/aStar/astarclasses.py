#!python3
# "#!python3" tells the computer what version of python to use when the file is run (e.g. when the I double click on the file in the windows explorer, or the desktop).
# More info on classes at:
# https://www.w3schools.com/python/python_classes.asp


import math     # To ensure ANGELS works on any machine, copy the actual math library into ANGELS' directory. math is used for math.inf, the value infinity.

class NodeClass:

    # # Number = math.inf       # Not being used
    # ScreenCoordinate = []
    # MatrixIndices = []
    # Origin = False
    # Destination = False
    # Null = True               # Null (empty) set.
    # Cue = False               # Open set. the remaining cue needs to be reset when aStar returns.
    # Visited = False           # Closed set. I'll need this to unset all of the visited and remaining cue nodes
    # Obstacle = False          # Restricted set.
    # Gcost = math.inf          # Distance from origin to current node.
    # Hcost = math.inf          # Distance from current node to destination.
    # Fcost = math.inf          # Total distance from origin to destination.
    # Parent = []               # The node previous to the current node (that has "found" the current node).

    # More info on empty/optional class constructors at:
    # https://stackoverflow.com/questions/42884795/python-empty-constructor
    def __init__(self,ScreenCoordinate=[],MatrixIndices=[],Origin=False,Destination=False,Null=True,Cue=False,Visited=False,Obstacle=False,Gcost=math.inf,Hcost=math.inf,Fcost=math.inf,Parent=[]): # __init__(): denotes that this function is a constructor

        # The following properties may need to be denoted as public if they're bein directly accesed throught the aStar() program.
        # self.Number = math.inf                    # Not being used
        self._ScreenCoordinate = ScreenCoordinate   # Leadnig underscore indicates, culturally, a private property of the class. This does not need to be done.
        self._MatrixIndices = MatrixIndices
        self._Origin = Origin
        self._Destination = Destination
        self._Null = Null                           # Null (empty) set.
        self._Cue = Cue                             # Open set. the remaining cue needs to be reset when aStar returns.
        self._Visited = Visited                     # Closed set. I'll need this to unset all of the visited and remaining cue nodes
        self._Obstacle = Obstacle                   # Restricted set.
        self._Gcost = Gcost
        self._Hcost = Hcost
        self._Fcost = Fcost
        self._Parent = Parent

    
    def resetNode(self):
        # self.Number = math.inf
        self.Origin = False
        self.Destination = False
        self.Null = True
        self.Cue = False
        self.Visited = False
        # The obstacle doesn't need to be reset since it was always false for this context.
        self.Gcost = math.inf
        self.Hcost = math.inf
        self.Fcost = math.inf
        self.Parent = []

    def setCue(self):
        self.Cue = True
        self.Null = False

    def setVisited(self):
        self.Visited = True
        self.Cue = False

    def setObstacle(self):
        self.Obstacle = True
        self.Null = False

    def unsetObstacle(self):
        self.Obstale = False
        self.Null = True






    # def getx0(self):
    #     return self.x0,self.y0
 
    # def setx0(self,x0):
        # self.x0 = x0

    # def addem(self):
    #     summy=self.getx0+self.gety0

    # @property
    # def x0(self):
    #     return(x0)
    
    # @property
    # def node(self):
    #     return(node)



# Things to remember:
# The ":" symbol indicates the start of a function, which is expecting tabbed statements by 4 spaces.
# The file __init__.py tells the computer that the class' file is in a valid location (I think that is what that does). Investigate linter and compiler.
# "self" is a keyword that is reserved for referencing the class instance itself. Any other name could be used, like "object" or "obj", but self is just a word that is reserved by Python to do this.







