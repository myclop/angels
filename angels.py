#!python3

# ANGELS
# Senior Design Project
# Authors: Jason Carruth, Nicholas Hampton, Mark Travers
#
# Fall 2018
#
# Top level for ANGELS base station

from scripts.utilities.messagepipe import MP
from scripts.controller.surveillance import Camera, SimulatedCamera
from scripts.controller.surveillance import Analyst
from scripts.controller.gvcontrol import GVPath
from scripts.controller.ui import GUI
from scripts.groundvehicle.gvmodels import BoeBotGV, SimulatedGV
from scripts.controller.gvcontrol import GVPilot

############################################ Operation ######################################################
# Switch between simulated and physical operation by (un)commenting appropriate lines only in this file.
# Run angels.py with Python 3
# Wait for camera and ground vehicle to be initialized
# Alter any computer vision or PID gains needed in the 'Settings' window
# Click a point on the 'User Navigation Control' window where the robot should travel
# With the 'User Navigation Control' window selected, press 'z' to enter fullscreen mode
# With the 'User Navigation Control' window selected, press 'x' to exit fullscreen mode
# Press 'Escape' at any time to terminate the system
#############################################################################################################

if __name__ == '__main__':
    # Initialize the message pipe by providing a list of desired message types and activity states (0-disabled, 1-enabled)
    # Any messages sent to the Message Pipe must provide a list of all message types which describe it. Only the
    #   enabled message types will be displayed on the terminal.
    initMessage = '*******  ANGELS System  *******'
    mp = MP(initMessage, dict({ 'All': 0,
                                'Status': 1,
                                'Non-Fatal Error': 1,
                                'Main': 1,
                                'Frame Counter': 0,
                                'Camera': 0,
                                'SimCamera': 0,
                                'Interpreter': 0,
                                'Pathing': 0,
                                'Pilot': 0,
                                'PID': 1,
                                'GV': 0,
                                'GUI': 0}))

    # Parameters (Matrix notation where applicable)
    navigationWindowResolution = (480, 640)
    # Refer to cv2.VideoCapture() documentation for cameraPort number
    cameraPort = 0
    cameraResolution = (480,640)
    # Initial orientation of the ground vehicle
    gvInitialCoords = (75,150)
    gvInitialHeading = 0.0
    # Maximum ground vehicle wheel effort
    gvTravelEffort = 1.0
    # Radius of the ground vehicle (pixels)
    gvPixelRadius = 35
    # RS232 port to ground vehicle
    commPort = 'COM3'
    baudRate = 9600

    # Controller Instantiations (Enable or disable SimulatedGV, BoeBotGV, SimulatedCamera, Camera to select operation mode)
    gui = GUI(mp, navigationWindowResolution, gvPixelRadius, gvInitialCoords, gvInitialHeading)
    jackRyan = Analyst(mp, gui, squareDimensions=(50, 60), cameraResolution=cameraResolution)
    chrisBurnett = GVPath(mp, gui, cameraResolution, pixelToNodeFactor=4)
    lilBot = SimulatedGV(mp, gui, initPosYX=gvInitialCoords, initHeading=gvInitialHeading)
    # lilBot = BoeBotGV(mp, gui, commPort, baudRate, 2)
    simBigBrother = SimulatedCamera(mp, gui, lilBot, './scripts/controller/cameraSimulatorData/', 'envReal.jpg', cameraResolution, gvInitialCoords)
    # bigBrother = Camera(mp, cameraPort, (640, 480), './scripts/controller/environmentData')
    redBaron = GVPilot(mp, gui, lilBot, chrisBurnett, gvTravelEffort)

    mp.out(['Main', 'Status'], 'Ready to start operation.')
    frameCounter = 0
    while True:
        # System active trackbar
        if gui.systemActive != 0:
            frameCounter += 1
            mp.out(['Frame Counter'], 'Frame: %d' % frameCounter)
            # Enable/Disable simBigBrother or bigBrother to select operation mode
            currentFrame = simBigBrother.captureFrame()
            # currentFrame = bigBrother.captureFrame()
            jackRyan.analyzeFrame(currentFrame)
            gui.updateWindows()
            # Plots a new path
            if gui.destCoords != (None, None) and gui.gvCoords != (None, None) and gui.needNewPath:
                chrisBurnett.findPath()
                gui.needNewPath = False
                redBaron.distanceErrorDerivative = 0.0
                redBaron.thetaErrorSum = 0.0
                redBaron.distanceErrorSum = 0.0
            # Moves ground vehicle 
            if gui.gvTraveling:
                redBaron.driveGV(gvTravelEffort)

        # Captures keystrokes and terminates system if user presses 'Escape'
        userKey = gui.captureUserKeys(timeout=1)
        if userKey == 27:
            break
