ANGELS
Senior Design Project
University of Colorado - Denver
Fall 2018

Mark Travers, Nicholas Hampton, Jason Carruth
For help beyond what is covered in the following and in program comments, contact Mark Travers (mark.2.travers@ucdenver.edu)

File Structure:
	./__pycache__ 				- Stores precompiled Python scripts
	./proto_algorithms 			- Stores Nick and Jason's codes for computer vision and aStar algorithm
	./scripts 					- Stores all controller libraries for ANGELS
	./.gitignore 				- Git ignore file
	./.pylintrc 				- Configuration for VSCode PyLint
	./__init__.py 				- Dummy python header
	./angels.py 				- Top-level script for ANGELS
	./README.txt 				- This file
	./venvPipRequirements.txt 	- Configuration script for Virtual Environment
	
ANGLES setup (Windows instructions):
	1. Clone and pull repository
	2. Install Python 3 (tested only on 3.7-64bit)
	3. Install Python Virtual Environment ("py -3 -m pip install virtualenv")
	4. Navigate to project folder ("cd pathToProject/angels")
	5. Create Python virtual environment ("py -3 -m virtualenv angelsvenv")
	6. Activate Python virtual environmnet (".\angelsvenv\Scripts\activate.bat")
	7. Install required libraries ("pip install -r venvPipRequirements.txt")
	(Follow these steps to use ANGELS with an XBEE-connected two-wheel robot)
	8. Install Digilent XCTU (https://www.digi.com/products/xbee-rf-solutions/xctu-software/xctu)
	9. Connect XBEE to computer via USB
	10. Configure XBEE with XCTU package (https://www.digi.com/products/xbee-rf-solutions/xctu-software/xctu)
	
ANGELS Operation:
	1. (Un)comment lines marked in 'angels.py' to activate simulated or physical hardware mode
	2. Alter 'angels.py' to (de)activate desired terminal message types
	3. Configure COMM PORT and CameraPort in 'angels.py' (if using physical hardware mode)
	4. Enable virtual environment (".\angelsvenv\Scripts\activate.bat")
	5. Start ANGELS ("python angels.py")
	6. Adjust trackbars on 'Settings' window for:
		a. Sys Active - (De)activates the system
		b. Edge Detection - Threshold value for Canny edge detection
		c. Safety Buffer - Width (pixels) of the safety buffer drawn around obstacles
		d. GV Radius - Radius (pixels) of the ground vehicle
		e. D-kp - P-gain for the distance PID
		f. D-ki - I-gain for the distance PID
		g. D-kd - D-gain for the distance PID
		h. Theta-kp - P-gain for the heading PID
		i. Theta-ki - I-gain for the heading PID
		j. Theta-kd - D-gain for the heading PID
		k. D% - distance PID mixing proportion
		l. Theta% - heading PID mixing proportion
		m. kh - Hardware gain (maps PID output to wheelspeeds)
	7. Click a non-obstacle spot on the 'User Navigation Control' window to move the ground vehicle
	(8.) Push 'z' to enter fullscreen, 'x' to exit fullscreen, and 'Escape' to terminate the program